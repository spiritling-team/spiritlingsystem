﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SpiritLingSystem.Library.Structures.Consts;

namespace SpiritLingSystem.Site.Account.Extensions
{
    public static class ServiceExtension 
    {
        // public static IServiceCollection AddSystemIdentityServer(this IServiceCollection services,IConfiguration configuration)
        // {
        //     var a = configuration.GetSection("IdentityServer:Clients").AsEnumerable();
        //     services.AddIdentityServer()
        //                     .AddDeveloperSigningCredential()
        //                     .AddInMemoryIdentityResources(IdentityServerConfig.IdentityResources)
        //                     .AddInMemoryApiScopes(IdentityServerConfig.ApiScopes)
        //                     .AddInMemoryClients(configuration.GetSection("IdentityServer:Clients"))
        //                     ;
        //     return services;
        // }

        public static IServiceCollection AddSystemIdentityServer(this IServiceCollection services,AppSettingConfig appsetting)
        {
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(appsetting.DataBaseSetting.IdentityServer);
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(appsetting.DataBaseSetting.IdentityServer);
                });

            return services;
        }
    }
}
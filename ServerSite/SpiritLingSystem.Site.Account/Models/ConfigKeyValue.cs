﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiritLingSystem.Site.Account.Models
{
    public class ConfigKeyValue
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Integrity { get; set; }
    }
}

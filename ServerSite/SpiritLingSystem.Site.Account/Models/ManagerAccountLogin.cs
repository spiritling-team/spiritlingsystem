﻿namespace SpiritLingSystem.Site.Account.Models
{
    public class ManagerAccountLogin
    {
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string LoginPassword { get; set; }
    }
}
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Library.Structures.Const.AppSettingConfigs;
using SpiritLingSystem.Site.IdentityServer.Extensions;

namespace SpiritLingSystem.Site.IdentityServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appSetting = Configuration.Get<AppSettingConfigPartial>();
            services.AddControllersWithViews();
            services.AddSingleton(Configuration);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            
            services.AddHttpContextAccessor();
            services.AddCustomDbContext(appSetting);
            services.AddStackExchangeRedisCache(options =>options.Configuration = appSetting.DataBaseSetting.Redis);
            services.AddLocalization();
            
            services.AddSystemIdentityServer(appSetting);
            services.AddLocalApiAuthentication();

            services.AddTryCore();
            services.AddUserSession(appSetting);
            services.AddBusinessInject();
            services.AddRepositoryInject();
            services.AddOtherInject();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseExceptionHandler("/Account/Error");

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            
            // TODO: 验证代理和直连ip获取
            // https://stackoverflow.com/questions/28664686/how-do-i-get-client-ip-address-in-asp-net-core
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAccessLog();

            app.UseIdentityServer();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Account}/{action=Index}/{id?}");
            });
        }
    }
}
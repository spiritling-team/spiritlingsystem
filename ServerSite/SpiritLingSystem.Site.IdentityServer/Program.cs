using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Site.IdentityServer.Extensions;

namespace SpiritLingSystem.Site.IdentityServer
{
    public class Program
    {
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
            .AddEnvironmentVariables()
            .Build();
        
        public static void Main(string[] args)
        {
            var appSetting = Configuration.Get<AppSettingConfigPartial>();
            // TODO: 想要添加Cookie字段到日志中去 - 在中间件添加
            Log.Logger = new LoggerConfiguration()
                .AddEnrich(Configuration)
                .WriteTo.AddDebug()
                .WriteTo.AddConsole()
                .WriteTo.AddLog2File(appSetting)
                .CreateLogger();
            try
            {
                Log.Information("Getting the service running...");
                CreateHostBuilder(args)
                    .Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}
﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.Site.IdentityServer.IdentityModels
{
    public partial class UserLoginHistory
    {
        public int Id { get; set; }
        public Guid UserKey { get; set; }
        public string Ip { get; set; }
        public string BrowserFingerprint { get; set; }
        public string UserAgent { get; set; }
        public DateTime CreatedTime { get; set; }

        public virtual User UserKeyNavigation { get; set; }
        public virtual BrowserInfo BrowserInfo { get; set; }
    }
}

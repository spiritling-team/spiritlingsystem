﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.Site.IdentityServer.IdentityModels
{
    public partial class BrowserInfo
    {
        public int HistoryId { get; set; }
        public string BrowserName { get; set; }
        public string BrowserVersion { get; set; }
        public string SystemName { get; set; }
        public string SystemVersion { get; set; }
        public string SystemVersionName { get; set; }
        public string Platform { get; set; }
        public string EngineName { get; set; }
        public string EngineVersion { get; set; }

        public virtual UserLoginHistory History { get; set; }
    }
}

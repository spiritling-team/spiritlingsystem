﻿
using SpiritLingSystem.Library.Tools.AttributeExtension;

namespace SpiritLingSystem.Site.IdentityServer.Consts
{
    public enum LoginResultType
    {
        [Information("用户名或者密码错误")]
        UserOrPwdInValid,
        [Information("未激活")]
        NotActivated,
        [Information("成功")]
        Success,
        [Information("需要验证码")]
        ShowCaptcha
    }
}
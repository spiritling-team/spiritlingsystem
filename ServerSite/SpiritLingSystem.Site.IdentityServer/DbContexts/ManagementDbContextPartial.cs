﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiritLingSystem.Site.IdentityServer.DbContexts
{
    public partial class ManagementDbContext : DbContext
    {
        public ManagementDbContext()
        {
        }

        public ManagementDbContext(DbContextOptions<ManagementDbContext> options)
            : base(options)
        {
        }
    }
}

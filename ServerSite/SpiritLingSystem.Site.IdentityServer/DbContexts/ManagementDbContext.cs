﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SpiritLingSystem.Site.IdentityServer.ManagementModels;

#nullable disable

namespace SpiritLingSystem.Site.IdentityServer.DbContexts
{
    public partial class ManagementDbContext : DbContext
    {
        public virtual DbSet<ApplicationConfig> ApplicationConfigs { get; set; }
        public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }
        public virtual DbSet<Sentence> Sentences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_PRC_CI_AS");

            modelBuilder.Entity<ApplicationConfig>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ApplicationConfig_pk")
                    .IsClustered(false);

                entity.ToTable("ApplicationConfig");

                entity.HasIndex(e => e.Id, "ApplicationConfig_Id_uindex")
                    .IsUnique();

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Memo).HasMaxLength(512);

                entity.Property(e => e.Type)
                    .HasDefaultValueSql("((1))")
                    .HasComment("0数字1字符串");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ApplicationSetting>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ApplicationSetting_pk")
                    .IsClustered(false);

                entity.ToTable("ApplicationSetting");

                entity.HasIndex(e => e.Id, "ApplicationSetting_Id_uindex")
                    .IsUnique();

                entity.Property(e => e.ConfigKey)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Memo).HasMaxLength(512);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(512);
            });

            modelBuilder.Entity<Sentence>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Sentence");

                entity.Property(e => e.Author)
                    .HasMaxLength(255)
                    .HasColumnName("author");

                entity.Property(e => e.Content)
                    .HasMaxLength(255)
                    .HasColumnName("content");

                entity.Property(e => e.Id).HasColumnName("id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiritLingSystem.Site.IdentityServer.DbContexts
{
    public partial class IdentityServerDbContext : DbContext
    {
        private readonly string _connectionString;

        public IdentityServerDbContext()
        {
        }

        /// <summary>
        /// 手动使用using形式使用
        /// </summary>
        /// <param name="connectionString"></param>
        public IdentityServerDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// 使用startup注入方式
        /// </summary>
        /// <param name="options"></param>
        public IdentityServerDbContext(DbContextOptions<IdentityServerDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    optionsBuilder.UseSqlServer(_connectionString);
                }
            }
        }
    }
}

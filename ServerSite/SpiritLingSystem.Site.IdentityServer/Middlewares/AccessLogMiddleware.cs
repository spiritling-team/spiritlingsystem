using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SpiritLingSystem.Framework.AspNetCore.Logger;

namespace SpiritLingSystem.Site.IdentityServer.Middlewares
{
    public class AccessLogMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IBusinessLogger<AccessLogMiddleware> _businessLogger;

        public AccessLogMiddleware(RequestDelegate next,IBusinessLogger<AccessLogMiddleware> businessLogger)
        {
            _next = next;
            _businessLogger = businessLogger;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            _businessLogger.LogInformation($"Method: {httpContext.Request.Method}, Path: {httpContext.Request.Path}");
            await _next.Invoke(httpContext);
        }
    }
}
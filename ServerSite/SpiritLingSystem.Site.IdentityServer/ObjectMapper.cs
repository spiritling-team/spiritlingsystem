﻿using AutoMapper;
using SpiritLingSystem.Library.Structures.Const.Cache;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;
using SpiritLingSystem.Site.IdentityServer.Models;

namespace SpiritLingSystem.Site.IdentityServer
{
    public class ObjectMapper:Profile
    {
        public ObjectMapper()
        {
            CreateMap<ClientBrowserInfo, BrowserInfo>()
                .ForMember(dest => dest.BrowserName, opt => opt.MapFrom(src => src.Browser.Name))
                .ForMember(dest => dest.BrowserVersion, opt => opt.MapFrom(src => src.Browser.Version))
                .ForMember(dest => dest.EngineName, opt => opt.MapFrom(src => src.Engine.Name))
                .ForMember(dest => dest.EngineVersion, opt => opt.MapFrom(src => src.Engine.Version))
                .ForMember(dest => dest.SystemName, opt => opt.MapFrom(src => src.Os.Name))
                .ForMember(dest => dest.SystemVersion, opt => opt.MapFrom(src => src.Os.Version))
                .ForMember(dest => dest.SystemVersionName, opt => opt.MapFrom(src => src.Os.VersionName))
                .ForMember(dest => dest.Platform, opt => opt.MapFrom(src => src.Platform.Type));
        }
    }
}
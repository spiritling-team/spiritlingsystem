﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SpiritLingSystem.Site.IdentityServer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Framework.AspNetCore.Repository;
using SpiritLingSystem.Library.Structures.Config;
using SpiritLingSystem.Library.Tools.EnumExtension;
using SpiritLingSystem.Library.Tools.StringExtension;
using SpiritLingSystem.Site.IdentityServer.Business;
using SpiritLingSystem.Site.IdentityServer.Consts;
using SpiritLingSystem.Site.IdentityServer.Filters;

namespace SpiritLingSystem.Site.IdentityServer.Controllers
{
    [SecurityHeaders]
    public class AccountController:Controller
    {
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly ILogger<AccountController> _logger;
        private readonly IAccountBusiness _business;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICommonBusiness _commonBusiness;
        
        public AccountController(ILogger<AccountController> logger, 
            IConfiguration configuration,
            IClientStore clientStore,
            IIdentityServerInteractionService interaction,
            IAccountBusiness business,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            IUserSessionRepository userSessionRepository,
            IManagementApplicationConfig applicationConfig,
            ICommonBusiness commonBusiness)
        {
            _logger = logger;
            _interaction = interaction;
            _business = business;
            _httpContextAccessor = httpContextAccessor;
            _commonBusiness = commonBusiness;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            // ManageSite https://localhost:44358/Account/Login?ReturnUrl=%2Fconnect%2Fauthorize%2Fcallback%3Fclient_id%3DManagerSite%26redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A12000%2Fcallback%26response_type%3Dcode%26scope%3Dopenid%20profile%20ManagerApi%26state%3D8ca8be8bba944cd295436d0ae20e7cc1%26code_challenge%3DtuXLHy6KKLYVhAjf6FwTTzspotIXDPUVpmdGL9heFuQ%26code_challenge_method%3DS256%26response_mode%3Dquery
            // BlogSite https://localhost:44358/Account/Login?ReturnUrl=%2Fconnect%2Fauthorize%2Fcallback%3Fclient_id%3DBlogSite%26redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A13000%2Fcallback%26response_type%3Dcode%26scope%3Dopenid%20profile%20BlogApi%26state%3D8ca8be8bba944cd295436d0ae20e7cc1%26code_challenge%3DtuXLHy6KKLYVhAjf6FwTTzspotIXDPUVpmdGL9heFuQ%26code_challenge_method%3DS256%26response_mode%3Dquery
            // state = 3D8ca8be8bba944cd295436d0ae20e7cc1
            // code_verifier = 27cc001caf2042f7935832b072551633c9db443642794f9795d32a107183e966d8c50f9f8c494aa682e171c10173e6ff
            // Code Challenge = tuXLHy6KKLYVhAjf6FwTTzspotIXDPUVpmdGL9heFuQ
            // Scope 可以使用逗号分割也可以使用空格分割
            // https://localhost:44358/Account/Login?ReturnUrl=%2Fconnect%2Fauthorize%2Fcallback%3Fclient_id%3DManagerSite%26redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A12000%2Fcallback%26response_type%3Dcode%26scope%3Dopenid%20profile%20ManagerApi%20offline_access%26state%3D8ca8be8bba944cd295436d0ae20e7cc1%26code_challenge%3DtuXLHy6KKLYVhAjf6FwTTzspotIXDPUVpmdGL9heFuQ%26code_challenge_method%3DS256%26response_mode%3Dquery
            // 可以拿到刷新token
            // 使用API Resource 配置后，无法直接使用，依旧是 scope形式，API Resource 是您的整体资源服务器，比如微服务状态下指定哪个api资源可以访问
            // https://stackoverflow.com/questions/44145760/identityserver4-apiresource-and-client-how-are-they-tied-together
            // https://www.cnblogs.com/edisonchou/p/integration_authentication-authorization_service_foundation.html 使用APIResource
            
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);

            if (!await _commonBusiness.ValidateUserAuthenticated(User))
            {
                await HttpContext.SignOutAsync();
            }
            return View(new ManagerAccountLogin()
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login(ManagerAccountLogin login)
        {
            var context = await _interaction.GetAuthorizationContextAsync(login.ReturnUrl);
            if (ModelState.IsValid)
            {
                
                var loginResult = await _business.AccountLogin(login);
                if (loginResult.ResultType == LoginResultType.Success)
                {
                    _logger.LogInformation("Login Success {@login}",login);
                    await HttpContext.SignOutAsync();
                    // 记录登录历史
                    await _commonBusiness.RecordUserLoginHistoryAsync(login as AccountLoginHistory, loginResult.User);
                    // 注册identityserver登录
                    await _commonBusiness.SignInUserAsync(loginResult.User,context);
                    if (login.ReturnUrl.IsNullOrWhiteSpace())
                    {
                        return Redirect("~/");
                    }
                    else
                    {
                        return Redirect(login.ReturnUrl);
                    }
                }
                else
                {
                    ModelState.AddModelError("CheckOverall", loginResult.ResultType.GetInformation());
                }
            }
            return View(login);
        }

        public async Task<IActionResult> Logout(string logoutId)
        {
            var context = await _interaction.GetLogoutContextAsync(logoutId);
            if (User != null)
            {
                var sessionId = User.GetSessionId();
                try
                {
                    await _commonBusiness.SignOutUserAsync(sessionId);    
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Logout {sessionId} occur error.", sessionId);
                }
            }

            if (context == null)
            {
                return View("Error",new AccountErrorModel()
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Message = "单点退出出现问题，请联系管理员 " + DateTime.Now.ToString()
                });
            }
            else
            {
                return Redirect(context.PostLogoutRedirectUri);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Error()
        {
            return View("Error",new AccountErrorModel()
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            });
        }
    }
}
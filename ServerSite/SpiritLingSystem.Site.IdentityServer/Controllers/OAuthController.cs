﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SpiritLingSystem.Site.IdentityServer.Controllers
{
    public class OAuthController:Controller
    {
        private const string CodingTeam = "spiritling";
        private readonly ILogger<OAuthController> _logger;
        

        public OAuthController(ILogger<OAuthController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Coding(string code)
        {
            // coding callback
            return View("_AccountLayout");
        }
    }
}
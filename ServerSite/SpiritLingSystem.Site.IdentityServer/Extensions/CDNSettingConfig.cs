namespace SpiritLingSystem.Site.IdentityServer.Extensions
{
    public class CDNSettingConfig
    {
        /// <summary>
        /// CDN 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 地址URL
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 完整性值验证
        /// </summary>
        public string Integrity { get; set; }
    }
}
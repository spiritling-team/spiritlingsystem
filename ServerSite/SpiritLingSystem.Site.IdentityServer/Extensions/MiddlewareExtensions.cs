using Microsoft.AspNetCore.Builder;
using SpiritLingSystem.Site.IdentityServer.Middlewares;

namespace SpiritLingSystem.Site.IdentityServer.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseAccessLog(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AccessLogMiddleware>();
        }
    }
}
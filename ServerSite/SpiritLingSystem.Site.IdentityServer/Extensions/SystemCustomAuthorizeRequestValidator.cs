﻿using System;
using System.Threading.Tasks;
using IdentityServer4.Extensions;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using SpiritLingSystem.Site.IdentityServer.Repository;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Site.IdentityServer.Business;

namespace SpiritLingSystem.Site.IdentityServer.Extensions
{
    public class SystemCustomAuthorizeRequestValidator:ICustomAuthorizeRequestValidator
    {
        /// <summary>
        /// The HTTP context accessor
        /// </summary>
        protected readonly IHttpContextAccessor HttpContextAccessor;

        private readonly ICommonBusiness _commonBusiness;

        /// <summary>
        /// Gets the HTTP context.
        /// </summary>
        /// <value>
        /// The HTTP context.
        /// </value>
        protected HttpContext HttpContext => HttpContextAccessor.HttpContext;

        public SystemCustomAuthorizeRequestValidator(
            IHttpContextAccessor httpContextAccessor,ICommonBusiness commonBusiness)
        {
            HttpContextAccessor = httpContextAccessor;
            _commonBusiness = commonBusiness;
        }
        
        public async Task ValidateAsync(CustomAuthorizeRequestValidationContext context)
        {
            var user = context.Result.ValidatedRequest.Subject;
            if (!await _commonBusiness.ValidateUserAuthenticated(user))
            {
                await HttpContext.SignOutAsync();
            }
        }
    }
}
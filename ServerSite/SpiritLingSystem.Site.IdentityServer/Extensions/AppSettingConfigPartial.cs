using System.Collections.Generic;
using SpiritLingSystem.Library.Structures.Const.AppSettingConfigs;

namespace SpiritLingSystem.Site.IdentityServer.Extensions
{
    public class AppSettingConfigPartial:AppSettingConfig
    {
        /// <summary>
        /// js cdn库
        /// </summary>
        public ICollection<CDNSettingConfig> JavaScript { get; set; } = new List<CDNSettingConfig>();
        /// <summary>
        /// css cdn库
        /// </summary>
        public ICollection<CDNSettingConfig> StyleSheet { get; set; } = new List<CDNSettingConfig>();
        
        public Certificates Certificates { get; set; }
    }

    public class Certificates
    {
        public string Path { get; set; }
        public string Password { get; set; }
    }
}
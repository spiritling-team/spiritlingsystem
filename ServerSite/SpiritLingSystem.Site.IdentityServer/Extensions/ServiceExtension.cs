﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using SpiritLingSystem.Framework.AspNetCore.Config;
using SpiritLingSystem.Framework.AspNetCore.Logger;
using SpiritLingSystem.Library.Structures.Const.AppSettingConfigs;
using SpiritLingSystem.Library.Structures.Config;
using SpiritLingSystem.Site.IdentityServer.Business;
using SpiritLingSystem.Site.IdentityServer.DbContexts;
using SpiritLingSystem.Site.IdentityServer.Repository;
using SpiritLingSystem.Site.IdentityServer.Extensions;

namespace SpiritLingSystem.Site.IdentityServer.Extensions
{
    public static class ServiceExtension 
    {
        public static IServiceCollection AddSystemIdentityServer(this IServiceCollection services,AppSettingConfigPartial appSetting)
        {
            string basePath = Directory.GetCurrentDirectory();
            //获取证书
            var certificate = new X509Certificate2(
                Path.Combine(basePath, appSetting.Certificates.Path),appSetting.Certificates.Password);
            services.AddIdentityServer(options =>
                {
                    options.Authentication.CookieLifetime = TimeSpan.FromDays(1);
                    options.Authentication.CookieSameSiteMode = Microsoft.AspNetCore.Http.SameSiteMode.Lax;
                    options.Authentication.CheckSessionCookieSameSiteMode = Microsoft.AspNetCore.Http.SameSiteMode.Lax;
                })
                .AddSigningCredential(certificate)
                .AddCustomAuthorizeRequestValidator<SystemCustomAuthorizeRequestValidator>()
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(appSetting.DataBaseSetting.IdentityServer);
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(appSetting.DataBaseSetting.IdentityServer);
                    options.EnableTokenCleanup = true;
                });

            return services;
        }

        public static IServiceCollection AddBusinessInject(this IServiceCollection services)
        {
            services.AddTransient<ICommonBusiness, CommonBusiness>();
            services.AddTransient<IAccountBusiness,AccountBusiness>();
            return services;
        }
        
        public static IServiceCollection AddRepositoryInject(this IServiceCollection services)
        {
            services.AddTransient<IUserRepository,UserRepository>();
            services.AddTransient<IUserLoginHistoryRepository, UserLoginHistoryRepository>();
            return services;
        }

        public static IServiceCollection AddOtherInject(this IServiceCollection services)
        {
            services.AddTransient<IManagementApplicationConfig, ManagementApplicationConfig>();
            services.AddTransient(typeof(IBusinessLogger<>),typeof(BusinessLogger<>));
            return services;
        }

        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, AppSettingConfigPartial appSetting)
        {
            services.AddDbContext<IdentityServerDbContext>(options =>
                options.UseSqlServer(appSetting.DataBaseSetting.IdentityServer));
            services.AddDbContext<ManagementDbContext>(options =>
                options.UseSqlServer(appSetting.DataBaseSetting.Management));

            return services;
        }
    }
}
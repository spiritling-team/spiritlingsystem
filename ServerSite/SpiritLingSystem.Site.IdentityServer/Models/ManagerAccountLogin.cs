﻿using System.ComponentModel.DataAnnotations;

namespace SpiritLingSystem.Site.IdentityServer.Models
{
    public class ManagerAccountLogin:AccountLoginHistory
    {
        /// <summary>
        /// 登录名
        /// </summary>
        [Required(ErrorMessage = "UserName is required")]
        public string LoginName { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        [Required(ErrorMessage = "Password is required")]
        public string LoginPassword { get; set; }
        
        public string ReturnUrl { get; set; }

        public bool IsError { get; set; } = false;
    }
}
﻿using IdentityServer4;
using SpiritLingSystem.Site.IdentityServer.Consts;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;

namespace SpiritLingSystem.Site.IdentityServer.Models
{
    public class LoginResult
    {
        public User User { get; set; }
        public LoginResultType ResultType { get; set; }

        public LoginResult()
        {
        }
        
        public LoginResult(LoginResultType type)
        {
            ResultType = type;
        }

        public LoginResult(User user):this(LoginResultType.Success)
        {
            User = user;
        }
    }
}
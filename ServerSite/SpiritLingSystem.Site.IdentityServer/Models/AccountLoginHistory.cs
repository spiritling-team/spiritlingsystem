﻿namespace SpiritLingSystem.Site.IdentityServer.Models
{
    public class AccountLoginHistory
    {
        public string BrowserInfo { get; set; }
        public string BrowserFingerprint { get; set; }
        
        public string UserAgent { get; set; }
    }
}
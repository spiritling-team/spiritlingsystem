namespace SpiritLingSystem.Site.IdentityServer.Models
{
    public class AccountErrorModel
    {
        public string RequestId { get; set; }
        public string Message { get; set; }
    }
}
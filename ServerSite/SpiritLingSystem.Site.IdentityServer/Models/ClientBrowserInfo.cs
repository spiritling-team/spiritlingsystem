﻿namespace SpiritLingSystem.Site.IdentityServer.Models
{
    public class ClientBrowserInfo
    {
        public Browser Browser { get; set; }
        public Engine Engine { get; set; }
        public Os Os { get; set; }
        public Platform Platform { get; set; }
    }
    
    public class Browser
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
        
    public class Engine
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
        
    public class Os
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string VersionName { get; set; }
    }
    public class Platform
    {
        public string Type { get; set; }
    }
}

// browser:
// name: "Chrome"
// version: "91.0.4472.106"
// __proto__: Object
// engine:
// name: "Blink"
// __proto__: Object
// os:
// name: "Windows"
// version: "NT 10.0"
// versionName: "10"
// __proto__: Object
// platform:
// type: "desktop"
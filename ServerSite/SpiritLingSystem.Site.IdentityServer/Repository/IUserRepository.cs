﻿using System.Threading.Tasks;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;

namespace SpiritLingSystem.Site.IdentityServer.Repository
{
    public interface IUserRepository
    {
        Task<User> FindUserByName(string loginName);
    }
}
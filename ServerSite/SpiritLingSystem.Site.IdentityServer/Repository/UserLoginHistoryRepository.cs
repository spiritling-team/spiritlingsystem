﻿using System.Threading.Tasks;
using SpiritLingSystem.Site.IdentityServer.DbContexts;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;

namespace SpiritLingSystem.Site.IdentityServer.Repository
{
    public class UserLoginHistoryRepository:IUserLoginHistoryRepository
    {
        private readonly IdentityServerDbContext _dbDbContext;

        public UserLoginHistoryRepository(IdentityServerDbContext dbDbContext)
        {
            _dbDbContext = dbDbContext;
        }
        public async Task InsertLoginHistory(UserLoginHistory loginHistory)
        {
            using (var t = _dbDbContext.Database.BeginTransaction())
            {
                try
                {
                    await _dbDbContext.UserLoginHistories.AddAsync(loginHistory);
                    await _dbDbContext.SaveChangesAsync();
                    await t.CommitAsync();
                }
                catch
                {
                    await t.RollbackAsync();
                }
                
            }
        }
    }
}
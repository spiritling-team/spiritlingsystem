﻿using System.Threading.Tasks;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;

namespace SpiritLingSystem.Site.IdentityServer.Repository
{
    public interface IUserLoginHistoryRepository
    {
        Task InsertLoginHistory(UserLoginHistory loginHistory);
    }
}
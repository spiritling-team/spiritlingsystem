﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SpiritLingSystem.Site.IdentityServer.DbContexts;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;
using Microsoft.EntityFrameworkCore;
using SpiritLingSystem.Library.Tools.StringExtension;

namespace SpiritLingSystem.Site.IdentityServer.Repository
{
    public class UserRepository:IUserRepository
    {
        private readonly IdentityServerDbContext _dbDbContext;

        public UserRepository(IdentityServerDbContext dbDbContext)
        {
            _dbDbContext = dbDbContext;
        }

        public async Task<User> FindUserByName(string loginName)
        {
            // linq中不允许使用comparisonType来进行翻译
            var user = await _dbDbContext.Users.Include(user => user.UserProfile).FirstOrDefaultAsync(u => u.UserProfile.Name.ToUpper() == loginName.ToUpper());
            return user;
        }
    }
}
﻿$(document).ready(function () {
    console.log("document ready");
    initFingerprintJS();

    const bowserInfo = bowser.getParser(window.navigator.userAgent)
    $("#BrowserInfo").val(JSON.stringify(bowserInfo.parsedResult));

    $("#UserAgent").val(navigator.userAgent);


    $("#LoginForm").submit(function () {
        var validate = $("#LoginForm").validate();
        if (validate.errorList.length > 0) {
            return false;
        } else {
            $("#submitBtn").attr("disabled", true);
        }
    })
})

function initFingerprintJS() {
    const fpPromise = FingerprintJS.load()

    // Get the visitor identifier when you need it.
    fpPromise
        .then(fp => fp.get())
        .then(result => {
            // This is the visitor identifier:
            let visitorId = result.visitorId;
            $("#BrowserFingerprint").val(visitorId);
        })
}
﻿using System;
using System.Threading.Tasks;
using SpiritLingSystem.Site.IdentityServer.Models;

namespace SpiritLingSystem.Site.IdentityServer.Business
{
    public interface IAccountBusiness
    {
        Task<LoginResult> AccountLogin(ManagerAccountLogin login);
    }
}
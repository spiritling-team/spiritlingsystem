﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4.Models;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;
using SpiritLingSystem.Site.IdentityServer.Models;

namespace SpiritLingSystem.Site.IdentityServer.Business
{
    public interface ICommonBusiness
    {
        /// <summary>
        /// 添加IdentityServerUser Claim
        /// </summary>
        /// <param name="claims"></param>
        /// <param name="user"></param>
        void AddUserClaim(ICollection<Claim> claims, User user);
        
        /// <summary>
        /// 记录用户登录历史
        /// </summary>
        /// <param name="accountLoginHistory"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task RecordUserLoginHistoryAsync(AccountLoginHistory accountLoginHistory, User user);

        Task SignInUserAsync(User user,AuthorizationRequest context);

        Task<bool> ValidateUserAuthenticated(ClaimsPrincipal user);

        Task SignOutUserAsync(Guid? sessionId);
    }
}
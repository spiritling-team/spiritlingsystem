﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Framework.AspNetCore.Models;
using SpiritLingSystem.Framework.AspNetCore.Repository;
using SpiritLingSystem.Library.Structures.Config;
using SpiritLingSystem.Library.Structures.Const.Application;
using SpiritLingSystem.Library.Structures.Const.IdentityServer;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;
using SpiritLingSystem.Site.IdentityServer.Models;
using SpiritLingSystem.Site.IdentityServer.Repository;

namespace SpiritLingSystem.Site.IdentityServer.Business
{
    public class CommonBusiness:ICommonBusiness
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IUserLoginHistoryRepository _userLoginHistoryRepository;
        private readonly ILogger<CommonBusiness> _logger;
        private readonly IManagementApplicationConfig _applicationConfig;
        private readonly IUserSessionRepository _userSessionRepository;
        
        public CommonBusiness(IHttpContextAccessor httpContextAccessor,IMapper mapper,IUserLoginHistoryRepository userLoginHistoryRepository,ILogger<CommonBusiness> logger,IManagementApplicationConfig applicationConfig,IUserSessionRepository userSessionRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _userLoginHistoryRepository = userLoginHistoryRepository;
            _logger = logger;
            _applicationConfig = applicationConfig;
            _userSessionRepository = userSessionRepository;
        }
        public void AddUserClaim(ICollection<Claim> claims, User user)
        {
            claims.Add(new Claim(SpiritLingClaimTypes.UserKey,user.Key.ToString()));
            claims.Add(new Claim(SpiritLingClaimTypes.Status,(Convert.ToInt32(user.UserProfile.Status)).ToString()));
            claims.Add(new Claim(SpiritLingClaimTypes.CreatedTime,user.UserProfile.CreatedTime.ToString("s")));
            claims.Add(new Claim(SpiritLingClaimTypes.UpdatedTime,user.UserProfile.UpdatedTime.ToString("s")));
        }

        public async Task RecordUserLoginHistoryAsync(AccountLoginHistory accountLoginHistory, User user)
        {
            var _ = JsonConvert.DeserializeObject<ClientBrowserInfo>(accountLoginHistory.BrowserInfo);
            var browserInfo = _mapper.Map<BrowserInfo>(_);
            // TODO: 反向代理和直连ip需要再一次确认
            var ip = _httpContextAccessor.HttpContext?.Connection.RemoteIpAddress?.MapToIPv4().ToString();
            var loginHistory = new UserLoginHistory()
            {
                UserKey = user.Key,
                Ip = ip,
                UserAgent = accountLoginHistory.UserAgent,
                BrowserFingerprint = accountLoginHistory.BrowserFingerprint,
                BrowserInfo = browserInfo
            };
            await _userLoginHistoryRepository.InsertLoginHistory(loginHistory);
        }

        public async Task SignInUserAsync(User user,AuthorizationRequest context)
        {
            var cookieExpiresTime =
                await _applicationConfig.GetConfigIntValueAsync(ApplicationConfigName.CookieSessionExpires);
            
            
            var props = new AuthenticationProperties
            {
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.Now.AddSeconds(cookieExpiresTime)
            };

            var sessionId = Guid.NewGuid();
            var identityUser = new IdentityServerUser(user.Key.ToString("N"));
            AddUserClaim(identityUser.AdditionalClaims,user);
            identityUser.AdditionalClaims.Add(new Claim(JwtClaimTypes.ClientId,
                context == null ? "Unknown" : context?.Client.ClientId));
            // 添加数据库对应的sessionID
            identityUser.AdditionalClaims.Add(new Claim(SpiritLingClaimTypes.SessionId,sessionId.ToString()));
            
            var userSession = new UserSession()
            {
                SessionId = sessionId,
                UserKey = user.Key,
                CreatedTime = DateTime.Now,
                Expiration = DateTime.Now.AddSeconds(cookieExpiresTime)
            };
            
            // 保存到redis缓存和数据库中
            await _userSessionRepository.AddUserSessionAsync(userSession);
            
            // 注册登录
            await _httpContextAccessor.HttpContext.SignInAsync(identityUser, props);
            
        }

        public async Task<bool> ValidateUserAuthenticated(ClaimsPrincipal user)
        {
            // 如果登录则直接重定向
            if (user?.Identity?.IsAuthenticated == true)
            {
                // 获取和判断session是否存在
                var sessionId = user.Identity.GetSessionId();
                if (sessionId.HasValue)
                {
                    // 先查缓存，再查数据库
                    var userSession = await _userSessionRepository.FindUserSessionByIdAsync(sessionId.Value);
                    if (userSession != null && DateTime.Compare(DateTime.Now, userSession.Expiration) != 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task SignOutUserAsync(Guid? sessionId)
        {
            if (sessionId != null)
            {
                // 删除数据库中记录
                await _userSessionRepository.RemoveUserSessionByIdAsync(sessionId.Value);
            }
            
            // 注册登录
            await _httpContextAccessor.HttpContext.SignOutAsync();
        }
    }
}
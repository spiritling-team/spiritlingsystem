﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SpiritLingSystem.Library.Structures.Const.IdentityServer;
using SpiritLingSystem.Library.Tools.StringExtension;
using SpiritLingSystem.Site.IdentityServer.Consts;
using SpiritLingSystem.Site.IdentityServer.IdentityModels;
using SpiritLingSystem.Site.IdentityServer.Models;
using SpiritLingSystem.Site.IdentityServer.Repository;

namespace SpiritLingSystem.Site.IdentityServer.Business
{
    public class AccountBusiness:IAccountBusiness
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserLoginHistoryRepository _userLoginHistoryRepository;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AccountBusiness(IUserRepository userRepository,IMapper mapper,IHttpContextAccessor httpContextAccessor,IUserLoginHistoryRepository userLoginHistoryRepository)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
            _userLoginHistoryRepository = userLoginHistoryRepository;
        }
        public async Task<LoginResult> AccountLogin(ManagerAccountLogin login)
        {
            var loginResult = new LoginResult();
            var user = await _userRepository.FindUserByName(login.LoginName.Trim());
            // 查找不到用户
            if (user == null || user?.UserProfile == null)
            {
                return new LoginResult(LoginResultType.UserOrPwdInValid);
            }

            // 查看用户是否激活
            if (!user.UserProfile.Status)
            {
                return new LoginResult(LoginResultType.NotActivated);
            }

            // 检查密码是否一致
            var currentPwd = login.LoginPassword.EncryptUser(user.UserProfile.Salt.ToString().ToUpper());
            if (!user.UserProfile.PassWord.EqualsIgnoreCase(currentPwd))
            {
                return new LoginResult(LoginResultType.UserOrPwdInValid);
            }

            return new LoginResult(user);
        }
    }
    
}
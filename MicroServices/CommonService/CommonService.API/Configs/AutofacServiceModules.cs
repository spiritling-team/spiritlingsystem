using System.Reflection;
using Autofac;
using CommonService.Core;

namespace CommonService.API.Configs
{
    public class AutofacServiceModules : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            // Core 核心方式
            var coreAssembly = typeof(CommonServiceCoreModule).GetTypeInfo().Assembly;
            // 注入IxxxBusiness 和 IxxxRepository
            builder.RegisterAssemblyTypes(coreAssembly)
                .Where(t => t.Name.EndsWith("Business") || t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces();
            
            // var businessAssembly = typeof(BusinessModule).GetTypeInfo().Assembly;
            // builder.RegisterAssemblyTypes(businessAssembly)
            //     .Where(t => t.Name.EndsWith("Business") || t.Name.EndWith("Repository"))
            //     .AsImplementedInterfaces();
        }
    }
}
using System;
using System.Linq;
using System.Threading.Tasks;
using CommonService.API.Controllers.Dto;
using CommonService.Core.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using SpiritLingSystem.Framework.AspNetCore;
using SpiritLingSystem.Framework.AspNetCore.Caching;
using SpiritLingSystem.Library.Structures.Const.Functionality;
using SpiritLingSystem.Library.Structures.ResultDTO;
using SpiritLingSystem.Library.Tools.Cache;

namespace CommonService.API.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class HomeDataController : ControllerBase
    {
        private readonly ILogger<HomeDataController> _logger;
        private readonly ISystemSession _systemSession;
        private readonly IFunctionalityBusiness _functionalityBusiness;
        private readonly IMenuBusiness _menuBusiness;
        private readonly IDistributedCache<SpiritLingUserCacheInfo> _cacheUserInfo;

        public HomeDataController(ILogger<HomeDataController> logger, ISystemSession systemSession,
            IFunctionalityBusiness functionalityBusiness, IMenuBusiness menuBusiness,
            IDistributedCache<SpiritLingUserCacheInfo> cacheUserInfo)
        {
            _logger = logger;
            _systemSession = systemSession;
            _functionalityBusiness = functionalityBusiness;
            _menuBusiness = menuBusiness;
            _cacheUserInfo = cacheUserInfo;
        }

        /// <summary>
        /// 获取用户对应的服务权限
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IResultDTO<HomeDataDto>> UserPermissions()
        {
            try
            {
                var userKey = _systemSession.UserKey;
                var clientId = _systemSession.ClientId;
                var functionalities =
                    await _functionalityBusiness.GetFunctionalitiesAsync(userKey, _systemSession.ServiceId);
                var menus = await _menuBusiness.GetMenusByFuncServiceAsync(functionalities, _systemSession.ServiceId);
                var cacheKey = SystemCacheUtils.UserClientPermission(clientId, userKey);
                var cacheUserInfo = new SpiritLingUserCacheInfo()
                {
                    Functionalities = functionalities.Select(x => x.Id).ToList()
                };
                await _cacheUserInfo.SetAsync(cacheKey, cacheUserInfo);
                return new ResultDTO<HomeDataDto>(new HomeDataDto()
                {
                    Menus = menus,
                    Functionalities = functionalities
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return ResultDTO<HomeDataDto>.InternalServerErrorResult();
            }
        }
    }
}
using System.Collections.Generic;
using CommonService.Core.ManagementModels;

namespace CommonService.API.Controllers.Dto
{
    public class HomeDataDto
    {
        public List<Menu> Menus { get; set; }
        public List<Functionality> Functionalities { get; set; }
    }
}
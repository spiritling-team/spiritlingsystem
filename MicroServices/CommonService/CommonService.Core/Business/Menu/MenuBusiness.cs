using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.ManagementModels;
using CommonService.Core.Repository;

namespace CommonService.Core.Business
{
    public class MenuBusiness : IMenuBusiness
    {
        private readonly IMenuRepository _repository;
        public MenuBusiness(IMenuRepository repository)
        {
            _repository = repository;
        }
        public async Task<List<Menu>> GetMenusByFuncServiceAsync(List<Functionality> functionalities, int serviceId)
        {
            return await _repository.GetMenusByFuncServiceAsync(functionalities, serviceId);
        }
    }
}
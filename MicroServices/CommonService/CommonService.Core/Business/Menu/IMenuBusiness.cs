using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.ManagementModels;

namespace CommonService.Core.Business
{
    public interface IMenuBusiness
    {
        Task<List<Menu>> GetMenusByFuncServiceAsync(List<Functionality> functionalities, int serviceId);
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.ManagementModels;

namespace CommonService.Core.Business
{
    public interface IFunctionalityBusiness
    {
        Task<List<Functionality>> GetFunctionalitiesAsync(Guid userKey, int serviceId);
    }
}
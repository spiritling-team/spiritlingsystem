using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.ManagementModels;
using CommonService.Core.Repository;
using Microsoft.Extensions.Logging;

namespace CommonService.Core.Business
{
    public class FunctionalityBusiness : IFunctionalityBusiness
    {
        private readonly ILogger<FunctionalityBusiness> _logger;
        private readonly IRoleRepository _roleRepository;
        private readonly IFunctionalityRepository _repository;

        public FunctionalityBusiness(ILogger<FunctionalityBusiness> logger,IRoleRepository roleRepository,IFunctionalityRepository repository)
        {
            _logger = logger;
            _roleRepository = roleRepository;
            _repository = repository;
        }

        public async Task<List<Functionality>> GetFunctionalitiesAsync(Guid userKey, int serviceId)
        {
            // TODO: 将权限和数据存到Redis中
            var roles = await _roleRepository.GetRoleListByUserKey(userKey);
            return await _repository.GetFunctionalityAsync(roles, serviceId);
        }
    }
}
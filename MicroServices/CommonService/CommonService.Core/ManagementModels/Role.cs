﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CommonService.Core.ManagementModels
{
    public partial class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
    }
}

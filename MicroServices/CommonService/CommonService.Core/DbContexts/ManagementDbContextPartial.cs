﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonService.Core.DbContexts
{
    public partial class ManagementDbContext : DbContext
    {
        private readonly string _connectionString;
        public ManagementDbContext()
        {
        }

        /// <summary>
        /// 手动使用using形式使用
        /// </summary>
        /// <param name="connectionString"></param>
        public ManagementDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ManagementDbContext(DbContextOptions<ManagementDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    optionsBuilder.UseSqlServer(_connectionString);
                }
            }
        }
    }
}

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonService.Core.DbContexts;
using CommonService.Core.ManagementModels;
using Microsoft.EntityFrameworkCore;
using SpiritLingSystem.Library.Tools.LinqExtension;
using Enumerable = System.Linq.Enumerable;

namespace CommonService.Core.Repository
{
    public class FunctionalityRepository:IFunctionalityRepository
    {
        private readonly ManagementDbContext _managementDbContext;
        public FunctionalityRepository(ManagementDbContext managementDbContext)
        {
            _managementDbContext = managementDbContext;
        }

        public async Task<List<Functionality>> GetFunctionalityAsync(List<Role> roles, int serviceId)
        {
            var roleIds = roles.Select(x => x.Id).ToList();
            // var rfs = await (from p in _managementDbContext.RoleFunctionalities
            //     join f in _managementDbContext.Functionalities on p.FunctionalityId equals f.Id
            //     where roleIds.Contains(p.RoleId)
            //     select p).AsNoTracking().ToListAsync();
            var rfs = await _managementDbContext.RoleFunctionalities.Include(x => x.Functionality)
                .Where(x => roleIds.Contains(x.RoleId)).ToListAsync();
            var funs = rfs.Select(x => x.Functionality).DistinctBy(x=>x.Id).OrderBy(x => x.Id).ToList();
            return funs.Where(x => x.DependenceServiceId == serviceId).ToList();
        }
    }
}
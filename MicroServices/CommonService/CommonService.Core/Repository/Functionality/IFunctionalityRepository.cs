using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.ManagementModels;

namespace CommonService.Core.Repository
{
    public interface IFunctionalityRepository
    {
        /// <summary>
        /// 获取角色对应的服务权限
        /// </summary>
        /// <param name="roles"></param>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        Task<List<Functionality>> GetFunctionalityAsync(List<Role> roles, int serviceId);
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.ManagementModels;

namespace CommonService.Core.Repository
{
    public interface IMenuRepository
    {
        Task<List<Menu>> GetMenusByFuncServiceAsync(List<Functionality> functionalities, int serviceId);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonService.Core.DbContexts;
using CommonService.Core.ManagementModels;
using Microsoft.EntityFrameworkCore;

namespace CommonService.Core.Repository
{
    public class MenuRepository : IMenuRepository
    {
        private readonly ManagementDbContext _managementDbContext;

        public MenuRepository(ManagementDbContext managementDbContext)
        {
            _managementDbContext = managementDbContext;
        }

        public async Task<List<Menu>> GetMenusByFuncServiceAsync(List<Functionality> functionalities, int serviceId)
        {
            var functionalityIds = functionalities.Select(x => x.Id);
            var menus = await (from p in _managementDbContext.Menus
                where (functionalityIds.Contains(p.FunctionalityId) || p.FunctionalityId == 0) && p.DependenceServiceId == serviceId
                select p).AsNoTracking().OrderBy(x=>x.Id).ToListAsync();
            return menus;
        }
    }
}
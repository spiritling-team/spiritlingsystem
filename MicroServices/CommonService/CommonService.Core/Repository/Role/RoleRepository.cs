using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.DbContexts;
using CommonService.Core.ManagementModels;
using Microsoft.EntityFrameworkCore;
using SpiritLingSystem.Library.Tools.LinqExtension;

namespace CommonService.Core.Repository
{
    public class RoleRepository:IRoleRepository
    {
        private readonly ManagementDbContext _managementDbContext;
        public RoleRepository(ManagementDbContext managementDbContext)
        {
            _managementDbContext = managementDbContext;
        }
        public async Task<List<Role>> GetRoleListByUserKey(Guid userKey)
        {
            var roles = await (from p in _managementDbContext.UserRoles
                where p.UserKey == userKey
                select p.Role).AsNoTracking().ToListAsync();
            return roles.DistinctBy(x=>x.Id).ToList();
        }
    }
}
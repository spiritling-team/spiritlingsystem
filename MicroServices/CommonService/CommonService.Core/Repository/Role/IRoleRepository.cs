using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonService.Core.ManagementModels;

namespace CommonService.Core.Repository
{
    public interface IRoleRepository
    {
        /// <summary>
        /// 获取用户不重复的角色
        /// </summary>
        /// <param name="userKey"></param>
        /// <returns></returns>
        Task<List<Role>> GetRoleListByUserKey(Guid userKey);
    }
}
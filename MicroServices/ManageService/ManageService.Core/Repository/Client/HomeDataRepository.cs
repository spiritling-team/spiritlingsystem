using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageService.Core.DbContexts;
using ManageService.Core.IdentityModels;
using ManageService.Core.ManagementModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpiritLingSystem.Library.Structures.ResultDTO;
using SpiritLingSystem.Library.Tools.LinqExtension;

namespace ManageService.Core.Repository
{
    public class ClientRepository:IClientRepository
    {
        private readonly ILogger<ClientRepository> _logger;
        private readonly ManagementDbContext _manageDbContext;
        private readonly IdentityServerDbContext _idsvDbContext;
        public ClientRepository(ILogger<ClientRepository> logger,ManagementDbContext manageDbContext,IdentityServerDbContext idsvDbContext)
        {
            _logger = logger;
            _manageDbContext = manageDbContext;
            _idsvDbContext = idsvDbContext;
        }

        public async Task<List<Client>> GetAllClients()
        {
            return await _idsvDbContext.Clients.AsNoTracking().ToListAsync();
        }

        public async Task<Client> GetClientDetail(int clientId)
        {
            return await _idsvDbContext.Clients.FirstOrDefaultAsync(x=>x.Id == clientId);
        }
    }
}
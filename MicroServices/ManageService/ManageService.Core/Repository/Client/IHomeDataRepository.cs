using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ManageService.Core.IdentityModels;
using ManageService.Core.ManagementModels;

namespace ManageService.Core.Repository
{
    public interface IClientRepository
    {
        Task<List<Client>> GetAllClients();
        Task<Client> GetClientDetail(int clientId);

    }
}
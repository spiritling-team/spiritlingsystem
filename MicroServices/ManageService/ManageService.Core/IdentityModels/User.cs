﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.IdentityModels
{
    public partial class User
    {
        public User()
        {
            UserLoginHistories = new HashSet<UserLoginHistory>();
            UserSessions = new HashSet<UserSession>();
        }

        public int Id { get; set; }
        public Guid Key { get; set; }
        public DateTime CreatedTime { get; set; }

        public virtual DoubleVerification DoubleVerification { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual ICollection<UserLoginHistory> UserLoginHistories { get; set; }
        public virtual ICollection<UserSession> UserSessions { get; set; }
    }
}

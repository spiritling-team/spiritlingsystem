﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.IdentityModels
{
    public partial class DoubleVerification
    {
        public Guid UserKey { get; set; }
        public Guid SecretKey { get; set; }
        public Guid? RestoreKey { get; set; }
        public DateTime CreatedTime { get; set; }

        public virtual User UserKeyNavigation { get; set; }
    }
}

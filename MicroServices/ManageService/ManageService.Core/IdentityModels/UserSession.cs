﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.IdentityModels
{
    public partial class UserSession
    {
        public Guid SessionId { get; set; }
        public Guid UserKey { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime Expiration { get; set; }

        public virtual User UserKeyNavigation { get; set; }
    }
}

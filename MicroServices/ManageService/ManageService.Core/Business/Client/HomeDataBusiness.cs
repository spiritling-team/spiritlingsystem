using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageService.Core.IdentityModels;
using ManageService.Core.Repository;
using Microsoft.Extensions.Logging;
using SpiritLingSystem.Framework.AspNetCore;

namespace ManageService.Core.Business
{
    public class ClientBusiness : IClientBusiness
    {
        private readonly ILogger<ClientBusiness> _logger;
        private readonly IClientRepository _repository;
        private readonly ISystemSession _systemSession;
        public ClientBusiness(ILogger<ClientBusiness> logger,IClientRepository repository,ISystemSession systemSession)
        {
            _logger = logger;
            _repository = repository;
            _systemSession = systemSession;
        }

        public async Task<List<Client>> GetAllClients()
        {
            return await _repository.GetAllClients();
        }

        public async Task<Client> GetClientDetail(int clientId)
        {
            return await _repository.GetClientDetail(clientId);
        }
    }
}
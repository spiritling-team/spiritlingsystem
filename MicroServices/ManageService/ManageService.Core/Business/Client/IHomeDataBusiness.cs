using System.Collections.Generic;
using System.Threading.Tasks;
using ManageService.Core.IdentityModels;

namespace ManageService.Core.Business
{
    public interface IClientBusiness
    {
        Task<List<Client>> GetAllClients();
        Task<Client> GetClientDetail(int clientId);
    }
}
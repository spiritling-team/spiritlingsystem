﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.ManagementModels
{
    public partial class RoleFunctionality
    {
        public Guid RoleId { get; set; }
        public int FunctionalityId { get; set; }

        public virtual Functionality Functionality { get; set; }
        public virtual Role Role { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.ManagementModels
{
    public partial class ApplicationConfig
    {
        public int Id { get; set; }
        public string ConfigKey { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Memo { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
    }
}

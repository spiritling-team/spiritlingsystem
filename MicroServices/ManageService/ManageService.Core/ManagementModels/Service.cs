﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.ManagementModels
{
    public partial class Service
    {
        public Service()
        {
            Functionalities = new HashSet<Functionality>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Functionality> Functionalities { get; set; }
    }
}

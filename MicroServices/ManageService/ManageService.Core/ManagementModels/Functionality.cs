﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.ManagementModels
{
    public partial class Functionality
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DependenceServiceId { get; set; }
        public int TheOrder { get; set; }
        public int? ParentId { get; set; }

        public virtual Service DependenceService { get; set; }
    }
}

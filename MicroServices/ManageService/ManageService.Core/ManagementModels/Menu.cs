﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.ManagementModels
{
    public partial class Menu
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string PageTitle { get; set; }
        public string Url { get; set; }
        public int FunctionalityId { get; set; }
        public int DependenceServiceId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ManageService.Core.ManagementModels
{
    public partial class UserRole
    {
        public Guid UserKey { get; set; }
        public Guid RoleId { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual Role Role { get; set; }
    }
}

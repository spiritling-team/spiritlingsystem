﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ManageService.Core.ManagementModels;

#nullable disable

namespace ManageService.Core.DbContexts
{
    public partial class ManagementDbContext : DbContext
    {
        public virtual DbSet<ApplicationConfig> ApplicationConfigs { get; set; }
        public virtual DbSet<Functionality> Functionalities { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleFunctionality> RoleFunctionalities { get; set; }
        public virtual DbSet<Sentence> Sentences { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_PRC_CI_AS");

            modelBuilder.Entity<ApplicationConfig>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ApplicationConfig_pk")
                    .IsClustered(false);

                entity.ToTable("ApplicationConfig");

                entity.HasIndex(e => e.Id, "ApplicationConfig_Id_uindex")
                    .IsUnique();

                entity.Property(e => e.ConfigKey)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Memo).HasMaxLength(512);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(512);
            });

            modelBuilder.Entity<Functionality>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Functionality_pk")
                    .IsClustered(false);

                entity.ToTable("Functionality");

                entity.HasIndex(e => e.Id, "Functionality_Id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(512);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TheOrder).HasDefaultValueSql("((10))");

                entity.HasOne(d => d.DependenceService)
                    .WithMany(p => p.Functionalities)
                    .HasForeignKey(d => d.DependenceServiceId)
                    .HasConstraintName("Functionality_Service_Id_fk");
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Menu_pk")
                    .IsClustered(false);

                entity.ToTable("Menu");

                entity.HasIndex(e => e.Id, "Menu_Id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(512);

                entity.Property(e => e.Icon).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PageTitle)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Url).HasMaxLength(512);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Role_pk")
                    .IsClustered(false);

                entity.ToTable("Role");

                entity.HasIndex(e => e.Id, "Role_Id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "Role_Name_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(current_request_id())");
            });

            modelBuilder.Entity<RoleFunctionality>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("RoleFunctionality");

                entity.HasOne(d => d.Functionality)
                    .WithMany()
                    .HasForeignKey(d => d.FunctionalityId)
                    .HasConstraintName("RoleServiceFunctionality_Functionality_Id_fk");

                entity.HasOne(d => d.Role)
                    .WithMany()
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("RoleServiceFunctionality_Role_Id_fk");
            });

            modelBuilder.Entity<Sentence>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Sentence");

                entity.Property(e => e.Author)
                    .HasMaxLength(255)
                    .HasColumnName("author");

                entity.Property(e => e.Content)
                    .HasMaxLength(255)
                    .HasColumnName("content");

                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Service_pk")
                    .IsClustered(false);

                entity.ToTable("Service");

                entity.HasIndex(e => e.Id, "Service_Id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("UserRole");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Role)
                    .WithMany()
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("UserRole_Role_Id_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

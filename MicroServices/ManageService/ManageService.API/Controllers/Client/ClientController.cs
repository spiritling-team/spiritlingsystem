using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ManageService.API.Controllers.Dto;
using ManageService.Core.Business;
using ManageService.Core.IdentityModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpiritLingSystem.Library.Structures.ResultDTO;

namespace ManageService.API.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class ClientController : ControllerBase
    {
        private readonly IClientBusiness _business;
        private readonly IMapper _mapper;

        public ClientController(IClientBusiness business, IMapper mapper)
        {
            _business = business;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IResultDTO<List<ClientDetail>>> List()
        {
            var clients = await _business.GetAllClients();
            var details = _mapper.Map<List<ClientDetail>>(clients);
            return new ResultDTO<List<ClientDetail>>(details);
        }

        [HttpGet("{clientId}")]
        public async Task<IResultDTO<Client>> Detail(int clientId)
        {
            var client = await _business.GetClientDetail(clientId);
            if (client != null)
            {
                return new ResultDTO<Client>(client);
            }
            return ResultDTO<Client>.BadRequestResult();
        }
    }
}
using AutoMapper;
using ManageService.API.Controllers.Dto;
using ManageService.Core.IdentityModels;

namespace ManageService.API.Configs
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<Client, ClientDetail>();
        }
    }
}
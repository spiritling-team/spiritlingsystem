using System;
using System.Collections.Generic;
using ManageService.Core.DbContexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace ManageService.API.Extensions
{
    public static class ManageServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, AppSettingConfigPartial configuration)
        {
            services.AddDbContext<ManagementDbContext>(options =>
                options.UseSqlServer(configuration.DataBaseSetting.Management));
            services.AddDbContext<IdentityServerDbContext>(optins => optins.UseSqlServer(configuration.DataBaseSetting.IdentityServer));
            return services;
        }
    }
}
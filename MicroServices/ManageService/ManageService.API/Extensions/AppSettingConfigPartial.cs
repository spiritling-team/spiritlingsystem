using System.Collections.Generic;
using SpiritLingSystem.Library.Structures.Const.AppSettingConfigs;

namespace ManageService.API.Extensions
{
    public class AppSettingConfigPartial:AppSettingConfig
    {
        public string Authority { get; set; }
        public List<string> Scopes { get; set; } = new List<string>();
    }
}
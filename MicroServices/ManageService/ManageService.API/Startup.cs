using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using ManageService.API.Configs;
using ManageService.API.Extensions;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SpiritLingSystem.Framework.AspNetCore;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Framework.AspNetCore.Logger;

namespace ManageService.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        private IConfiguration Configuration { get; set; }
        private ILifetimeScope AutofacContainer { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            IdentityModelEventSource.ShowPII = true;
            var appSetting = Configuration.Get<AppSettingConfigPartial>();
            // Controller 设置
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                };
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            services.AddSingleton(Configuration);
            services.AddHttpContextAccessor();
            
            services.AddTryCore();
            services.AddCustomAuthentication(appSetting.Authority);
            services.AddCustomAuthorization(appSetting.Scopes);
            services.AddCustomDbContext(appSetting);
            services.AddCustomRedisCache(appSetting.DataBaseSetting.Redis);
            services.AddAutoMapper(typeof(MapperProfile));
            services.AddCustomSwaggerGen("ManageService");
            
        }
        
        public static void ConfigureContainer(ContainerBuilder builder)
        {
            // Register your own things directly with Autofac, like:
            builder.RegisterModule<AutofacServiceModules>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ManageService.API v1"));
            }

            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            // app.UseHttpsRedirection();

            app.UseRouting();
            // app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

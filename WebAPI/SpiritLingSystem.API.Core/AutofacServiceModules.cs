﻿using System.Reflection;
using Autofac;

namespace SpiritLingSystem.API.Core
{
    public class AutofacServiceModules : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            // Core 核心方式
            //var coreAssembly = typeof(CoreModule).GetTypeInfo().Assembly;
            //builder.RegisterAssemblyTypes(coreAssembly)
            //    .AsImplementedInterfaces();
            //
            // var businessAssembly = typeof(BusinessModule).GetTypeInfo().Assembly;
            // builder.RegisterAssemblyTypes(businessAssembly)
            //     .Where(t => t.Name.EndsWith("Business"))
            //     .AsImplementedInterfaces();
        }
    }
}
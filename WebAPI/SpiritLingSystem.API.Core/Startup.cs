using System;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SpiritLingSystem.Framework.AspNetCore.Extension;

namespace SpiritLingSystem.API.Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; set; }
        private ILifetimeScope AutofacContainer { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // 添加CORS
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    // AllowCredentials 需要指定具体的origin才可以使用
                    builder.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin();
                });
            });
            // Controller 设置
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                };
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            
            // TODO: IConfiguration 使用autofac注入方式
            services.AddSingleton(Configuration);
            // TODO: IHttpContextAccessor 使用autofac注入方式
            services.AddHttpContextAccessor();
            services.AddCustomRedisCache(Configuration["Redis"]);
            // TODO: 添加版本控制器，目前版本可以控制，但是swagger配合版本控制无法调试使用
            services.AddSwaggerGen();

            // services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            // .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme,options =>
            // {
            //     options.RequireHttpsMetadata = false;
            //     options.Authority = "https://localhost:44358";
            //     options.TokenValidationParameters = new TokenValidationParameters
            //     {
            //         ValidateIssuer = false,//验证
            //         ValidateAudience = false,//
            //         ClockSkew = TimeSpan.FromSeconds(30),
            //     };
            // });
            //
            // services.AddAuthorization(options =>
            // {
            //     options.AddPolicy("apiScope", policy =>
            //     {
            //         policy.RequireAuthenticatedUser();
            //         policy.RequireClaim("scope", "ManagerApi");
            //     });
            // });
        }

        public static void ConfigureContainer(ContainerBuilder builder)
        {
            // Register your own things directly with Autofac, like:
            builder.RegisterModule<AutofacServiceModules>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "SpiritLingSystem.ManageService.API v1"); });
            }

            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            //会影响网关处理
            // app.UseHttpsRedirection();
            app.UseRouting();
            // app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
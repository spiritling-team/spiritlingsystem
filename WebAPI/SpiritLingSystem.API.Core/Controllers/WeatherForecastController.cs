﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog;
using SpiritLingSystem.Library.Structures.ResultDTO;

namespace SpiritLingSystem.API.Core.Controllers
{
    [ApiController]
    //[Authorize]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IDistributedCache _distributedCache;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,IDistributedCache distributedCache)
        {
            _logger = logger;
            _distributedCache = distributedCache;
        }

        [HttpGet]
        public IResultDTO<IEnumerable<WeatherForecast>> Get()
        {
            // 添加新的参数 LogContext.PushProperty("UserName", "admin");
            var rng = new Random();
            
            // _logger.LogTrace("Trace"); // Verbose
            // _logger.LogDebug("Debug");
            // _logger.LogInformation("Information");
            // _logger.LogWarning("Warning");
            // _logger.LogError("Error");
            // _logger.LogCritical("Critical"); // == Fatal
            
            var arr = Enumerable.Range(1, 5).Select(index => new WeatherForecast
                {
                    Date = DateTime.Now.AddDays(index),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                })
                .ToArray();;
            var str = JsonConvert.SerializeObject(arr);
            // _distributedCache.Set("appsettingList",Encoding.UTF8.GetBytes(str));
            return new ResultDTO<IEnumerable<WeatherForecast>>(arr);
        }
    }
}
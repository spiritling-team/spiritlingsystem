using System.Collections.Generic;
using SpiritLingSystem.Library.Structures.Const.AppSettingConfigs;

namespace SpiritLingSystem.Gateway.Extensions
{
    public class AppSettingConfigPartial:AppSettingConfig
    {
        public string Authority { get; set; }
        public List<string> AllowedOrigins { get; set; } = new List<string>();
    }
}
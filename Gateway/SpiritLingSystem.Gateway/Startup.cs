using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Ocelot.Configuration;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Serilog;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Framework.AspNetCore.Repository;
using SpiritLingSystem.Gateway.Extensions;
using SpiritLingSystem.Library.Structures.Const.IdentityServer;

namespace SpiritLingSystem.Gateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var appsetting = Configuration.Get<AppSettingConfigPartial>();

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    // AllowCredentials 需要指定具体的origin才可以使用
                    builder.WithOrigins(appsetting.AllowedOrigins.ToArray()).AllowAnyMethod().AllowAnyHeader();
                });
            });
            services.AddHttpContextAccessor();
            services.AddCustomRedisCache(appsetting.DataBaseSetting.Redis);
            services.AddUserSession(appsetting);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer("SpiritLingSystemApi", options =>
                {
                    options.RequireHttpsMetadata = true;
                    options.Authority = appsetting.Authority;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        ClockSkew = TimeSpan.FromSeconds(30),
                    };
                });
            services.AddOcelot(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors();

            var config = new OcelotPipelineConfiguration()
            {
                AuthenticationMiddleware = LeySerFujiAuthenticationMiddleware
            };

            //app.UseOcelot(configuration).Wait();
            app.UseAuthentication()
                .UseOcelot(config).Wait();
            //app.UseOcelot();
        }

        private async Task LeySerFujiAuthenticationMiddleware(HttpContext httpContext, Func<Task> next)
        {
            try
            {
                var downstreamRoute = httpContext.Items.DownstreamRoute();


                if (httpContext.Request.Method.ToUpper() == "OPTIONS")
                {
                    Log.Information(
                        $"Client request {httpContext.Request.Path} and method {httpContext.Request.Method.ToUpper()}");
                    await next.Invoke();
                    return;
                }

                if (IsAuthenticatedRoute(downstreamRoute))
                {
                    Log.Information(
                        $"{httpContext.Request.Path} is an authenticated route. PreAuthenticationMiddleware checking if client is authenticated");

                    var result =
                        await httpContext.AuthenticateAsync(downstreamRoute.AuthenticationOptions
                            .AuthenticationProviderKey);

                    var allowScopes = downstreamRoute.AuthenticationOptions.AllowedScopes;

                    httpContext.User = result?.Principal;

                    if (result?.Principal != null)
                    {
                        var userSessionRepository = httpContext.RequestServices.GetService<IUserSessionRepository>();
                        //var sub = result.Principal.Identity.GetSubjectId();
                        var session = result.Principal.Identity.GetSessionId();
                        var userSession = await userSessionRepository.FindUserSessionByIdAsync(session.Value);
                        if (userSession != null)
                        {
                            var jwtScopes = result.Principal.Claims
                                .Where(x => x.Type == "scope" && allowScopes.Contains(x.Value)).ToList();
                            // httpContext.User.Identity 可以获取scope
                            // scope:profile scope:ManagerApi
                            if (httpContext.User?.Identity?.IsAuthenticated == true &&
                                jwtScopes.Count == allowScopes.Count)
                            {
                                Log.Information(
                                    $"Client has been authenticated for {httpContext.Request.Path} by  SubjectId");
                                await next.Invoke();
                                return;
                            }
                        }

                        var userKey = result.Principal.Identity.GetUserKey();
                        var error = new UnauthenticatedError(
                            $"Request for authenticated route {httpContext.Request.Path} by UserKey: {userKey} and session: {session} was unauthenticated by SubjectId");
                        Log.Information(
                            $"Client has NOT been authenticated UserKey: {userKey} and session: {session} for {httpContext.Request.Path} and method {httpContext.Request.Method.ToUpper()} pipeline error set. {error}");
                        httpContext.Response.StatusCode = 401;
                        httpContext.Items.SetError(error);
                        return;
                    }
                    else
                    {
                        var error = new UnauthenticatedError(
                            $"Request for authenticated route {httpContext.Request.Path} not authorization bearer token");
                        Log.Information(
                            $"Client has NOT been authenticated for {httpContext.Request.Path} and method {httpContext.Request.Method.ToUpper()} pipeline error set. {error}");
                        httpContext.Response.StatusCode = 401;
                        httpContext.Items.SetError(error);
                        return;
                    }
                }

                Log.Information(
                    $"Client request {httpContext.Request.Path} is not authenticated and method {httpContext.Request.Method.ToUpper()}");
                await next.Invoke();
                return;
            }
            catch (Exception e)
            {
                Log.Error(e, "Gateway Error");
            }
        }

        private static bool IsAuthenticatedRoute(DownstreamRoute route)
        {
            return route.IsAuthenticated;
        }
    }
}
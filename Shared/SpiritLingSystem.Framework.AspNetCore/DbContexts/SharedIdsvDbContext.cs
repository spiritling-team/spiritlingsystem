using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SpiritLingSystem.Framework.AspNetCore.Models;

namespace SpiritLingSystem.Framework.AspNetCore.DbContexts
{
    public partial class SharedIdsvDbContext : DbContext
    {
        public virtual DbSet<UserSession> UserSessions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_PRC_CI_AS");
            
            modelBuilder.Entity<UserSession>(entity =>
            {
                entity.HasKey(e => e.SessionId)
                    .HasName("UserSession_pk")
                    .IsClustered(false);

                entity.ToTable("UserSession");

                entity.HasIndex(e => e.SessionId, "UserSession_SessionId_uindex")
                    .IsUnique();

                entity.Property(e => e.SessionId).ValueGeneratedNever();

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Expiration).HasColumnType("datetime");
            });
        }
    }
}
using Microsoft.EntityFrameworkCore;

namespace SpiritLingSystem.Framework.AspNetCore.DbContexts
{
    public partial class SharedIdsvDbContext : DbContext
    {
        private readonly string _connectionString;

        public SharedIdsvDbContext()
        {
        }

        /// <summary>
        /// 手动使用using形式使用
        /// </summary>
        /// <param name="connectionString"></param>
        public SharedIdsvDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// 使用startup注入方式
        /// </summary>
        /// <param name="options"></param>
        public SharedIdsvDbContext(DbContextOptions<SharedIdsvDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    optionsBuilder.UseSqlServer(_connectionString);
                }
            }
        }
    }
}
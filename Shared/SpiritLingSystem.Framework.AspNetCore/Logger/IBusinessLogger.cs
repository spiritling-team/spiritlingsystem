using System;
using Microsoft.Extensions.Logging;

namespace SpiritLingSystem.Framework.AspNetCore.Logger
{
    public interface IBusinessLogger<T>
    {
        void LogInformation(EventId eventId, Exception exception, string message, params object[] args);
        void LogInformation(Exception exception, string message, params object[] args);
        void LogInformation(string message, params object[] args);
    }
}
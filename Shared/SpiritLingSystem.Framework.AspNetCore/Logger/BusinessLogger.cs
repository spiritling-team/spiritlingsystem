using System;
using Microsoft.Extensions.Logging;
using Serilog.Context;

namespace SpiritLingSystem.Framework.AspNetCore.Logger
{
    public class BusinessLogger<T>:IBusinessLogger<T>
    {
        private readonly ILogger<T> _logger;
        private const string LogType = "Bussiness";
        
        public BusinessLogger(ILogger<T> logger)
        {
            _logger = logger;
        }

        private void SetProperty()
        {
            LogContext.PushProperty("SourceContext", LogType);
        }
        public void LogInformation(EventId eventId, Exception exception, string message, params object[] args)
        {
            SetProperty();
            _logger.LogInformation(eventId, exception, message, args);
        }

        public void LogInformation(Exception exception, string message, params object[] args)
        {
            SetProperty();
            _logger.LogInformation(exception, message, args);
        }

        public void LogInformation(string message, params object[] args)
        {
            SetProperty();
            _logger.LogInformation(message, args);
        }
    }
}
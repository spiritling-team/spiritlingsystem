using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SpiritLingSystem.Framework.AspNetCore.Caching;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using SpiritLingSystem.Library.Structures.Const.Functionality;
using SpiritLingSystem.Library.Tools.Cache;

namespace SpiritLingSystem.Framework.AspNetCore
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false)]
    public class FunctionalityAuthorizeAttribute: Attribute,IAuthorizationFilter
    {
        private readonly List<int> _functionality;
        public FunctionalityAuthorizeAttribute(params int[] functionality)
        {
            _functionality = functionality.ToList();
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var httpContext = context.HttpContext;
            var services = context.HttpContext.RequestServices;
            var logger = services.GetService<ILogger<FunctionalityAuthorizeAttribute>>();
            try
            {
                var userKey = httpContext.User.Identity.GetUserKey();
                var clientId = httpContext.User.Identity.GetClientId()?.ToLower();
                if (userKey.HasValue)
                {
                    var disCache = services.GetService<IDistributedCache<SpiritLingUserCacheInfo>>();
                    var userCacheInfo = disCache.Get(SystemCacheUtils.UserClientPermission(clientId,userKey.Value));
                    var functionalityIntersect = userCacheInfo.Functionalities.Intersect(_functionality).ToList();
                    if (functionalityIntersect.Count == _functionality.Count)
                    {
                        logger.LogInformation($"Request Functionality Success: [{string.Join(",",_functionality)}]");
                        return;
                    }
                }

                logger.LogInformation($"Request Functionality Failed: [{string.Join(",",_functionality)}]");
                context.Result = new StatusCodeResult((int) HttpStatusCode.Forbidden);
                return;
            }
            catch (Exception e)
            {
                logger.LogError("Functionality Authorize Error: ",e);
                context.Result = new StatusCodeResult((int) HttpStatusCode.Forbidden);
                return;
            }
            
        }
    }
}
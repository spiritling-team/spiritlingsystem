using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace SpiritLingSystem.Framework.AspNetCore.Caching
{
    public interface IDistributedCache<TCacheItem>
    {
        TCacheItem Get(string key);

        Task<TCacheItem> GetAsync(string key, CancellationToken token = default);
        
        void Set(
            string key,
            TCacheItem value,
            DistributedCacheEntryOptions options = null
        );

        Task SetAsync(
            string key,
            TCacheItem value,
            DistributedCacheEntryOptions options = null,
            CancellationToken token = default
        );

        void Remove(string key);

        Task RemoveAsync(string key, CancellationToken token = default);
    }
}
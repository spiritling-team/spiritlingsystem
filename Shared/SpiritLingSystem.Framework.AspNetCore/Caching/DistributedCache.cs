using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SpiritLingSystem.Library.Structures.Const.Cache;

namespace SpiritLingSystem.Framework.AspNetCore.Caching
{
    public class DistributedCache<TCacheItem> : IDistributedCache<TCacheItem> where TCacheItem : class
    {
        protected IDistributedCache Cache { get; }
        protected DistributedCacheEntryOptions DefaultCacheOptions;
        protected string CacheName { get; set; }
        protected ILogger Logger;
        protected SemaphoreSlim SyncSemaphore { get; }


        public DistributedCache(IDistributedCache cache, ILogger<DistributedCache<TCacheItem>> logger)
        {
            Cache = cache;
            CacheName = typeof(TCacheItem).Name.ToLower();
            DefaultCacheOptions = new DistributedCacheEntryOptions();
            Logger = logger;
            SyncSemaphore = new SemaphoreSlim(1, 1);
        }

        protected virtual string NormalizeKey(string key)
        {
            return $"{CacheNamespace.SystemManagement}{CacheName}:{key.ToLower()}";
        }

        public TCacheItem Get(string key)
        {
            string cachedString;
            try
            {
                cachedString = Cache.GetString(NormalizeKey(key));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Get cache item ({CacheKey}) error. ", NormalizeKey(key));
                return null;
                //throw;
            }

            if (string.IsNullOrEmpty(cachedString))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<TCacheItem>(cachedString);
        }

        public async Task<TCacheItem> GetAsync(string key, CancellationToken token = default)
        {
            string cachedString;
            try
            {
                cachedString = await Cache.GetStringAsync(NormalizeKey(key));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Get cache item ({CacheKey}) error. ", NormalizeKey(key));
                return null;
            }

            if (string.IsNullOrEmpty(cachedString))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<TCacheItem>(cachedString);
        }

        public void Set(string key, TCacheItem value, DistributedCacheEntryOptions options = null)
        {
            try
            {
                Cache.SetString(
                    NormalizeKey(key),
                    JsonConvert.SerializeObject(value),
                    options ?? DefaultCacheOptions
                );
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Set cache item ({CacheKey} - {@CacheItem}) error. ", NormalizeKey(key), value);
            }
        }

        public async Task SetAsync(string key, TCacheItem value, DistributedCacheEntryOptions options = null,
            CancellationToken token = default)
        {
            try
            {
                await Cache.SetStringAsync(
                    NormalizeKey(key),
                    JsonConvert.SerializeObject(value),
                    options ?? DefaultCacheOptions
                );
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Set cache item ({CacheKey} - {@CacheItem}) error. ", NormalizeKey(key), value);
            }
        }

        public void Remove(string key)
        {
            try
            {
                Cache.Remove(NormalizeKey(key));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Remove cache item ({CacheKey}) error. ", NormalizeKey(key));
            }
        }

        public async Task RemoveAsync(string key, CancellationToken token = default)
        {
            try
            {
                await Cache.RemoveAsync(NormalizeKey(key));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Remove cache item ({CacheKey}) error. ", NormalizeKey(key));
            }
        }
    }
}
﻿using Microsoft.AspNetCore.Http;
using SpiritLingSystem.Framework.AspNetCore.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SpiritLingSystem.Library.Structures.Const.IdentityServer;

namespace SpiritLingSystem.Framework.AspNetCore
{
    public class SystemSession: ISystemSession
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClaimsPrincipal Principal => _httpContextAccessor.HttpContext?.User;
        public SystemSession(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        
        public Guid UserKey => Principal.GetUserKey();

        public string ClientId => Principal.GetClientId();

        public int ServiceId
        {
            get
            {
                var exist = Enum.TryParse<SpiritLingClientIdEnum>(ClientId,out var result);
                if (exist)
                {
                    return (int)result;
                }

                return 0;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.Framework.AspNetCore
{
    public interface ISystemSession
    {
        public Guid UserKey { get; }
        
        public string ClientId { get; }
        
        public int ServiceId { get; }
    }
}

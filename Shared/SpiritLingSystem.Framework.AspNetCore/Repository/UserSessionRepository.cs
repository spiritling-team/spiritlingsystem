using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using SpiritLingSystem.Framework.AspNetCore.Caching;
using SpiritLingSystem.Framework.AspNetCore.DbContexts;
using SpiritLingSystem.Framework.AspNetCore.Models;

namespace SpiritLingSystem.Framework.AspNetCore.Repository
{
    public class UserSessionRepository : IUserSessionRepository
    {
        private readonly SharedIdsvDbContext _dbDbContext;
        private readonly ILogger<UserSessionRepository> _logger;
        private readonly IDistributedCache<UserSession> _cacheUserSession;

        public UserSessionRepository(ILogger<UserSessionRepository> logger, SharedIdsvDbContext dbContext,
            IDistributedCache<UserSession> cacheUserSession)
        {
            _logger = logger;
            _dbDbContext = dbContext;
            _cacheUserSession = cacheUserSession;
        }

        public async Task AddUserSessionAsync(UserSession userSession)
        {
            await _cacheUserSession.SetAsync(userSession.SessionId.ToString("N"), userSession,
                new DistributedCacheEntryOptions()
                {
                    AbsoluteExpiration = userSession.Expiration
                });

            try
            {
                await _dbDbContext.UserSessions.AddAsync(userSession);
                await _dbDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Set session to sql server item (SessionId: {0}, {@userSession}) error. ",
                    userSession.SessionId.ToString(), userSession);
            }
        }

        public void AddUserSession(UserSession userSession)
        {
            _cacheUserSession.Set(userSession.SessionId.ToString("N"), userSession,
                new DistributedCacheEntryOptions()
                {
                    AbsoluteExpiration = userSession.Expiration
                });

            try
            {
                _dbDbContext.UserSessions.Add(userSession);
                _dbDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Set session to sql server item (SessionId: {0}, {@userSession}) error. ",
                    userSession.SessionId.ToString(), userSession);
            }
        }

        public async Task<UserSession> FindUserSessionByIdAsync(Guid sessionId)
        {
            try
            {
                var cache = await GetSessionForRedisAsync(sessionId);
                if (cache == null)
                {
                    var userSession = await _dbDbContext.UserSessions.FirstOrDefaultAsync(s => s.SessionId == sessionId);
                    if (userSession != null)
                    {
                        // 从数据库获取后，将数据存到缓存中一份
                        // 从数据库获取后，将数据存到缓存中一份
                        await _cacheUserSession.SetAsync(userSession.SessionId.ToString("N"), userSession,
                            new DistributedCacheEntryOptions()
                            {
                                AbsoluteExpiration = userSession.Expiration
                            });
                        return userSession;
                    }
                }

                return cache;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get session for sql server and redis item (SessionId: {0}) error. ",
                    sessionId);
                return null;
            }
        }

        public UserSession FindUserSessionById(Guid sessionId)
        {
            try
            {
                var cache = GetSessionForRedis(sessionId);
                if (cache == null)
                {
                    var userSession = _dbDbContext.UserSessions.FirstOrDefault(s => s.SessionId == sessionId);
                    if (userSession != null)
                    {
                        // 从数据库获取后，将数据存到缓存中一份
                        _cacheUserSession.Set(userSession.SessionId.ToString("N"), userSession,
                            new DistributedCacheEntryOptions()
                            {
                                AbsoluteExpiration = userSession.Expiration
                            });
                        return userSession;
                    }
                }

                return cache;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get session for sql server and redis item (SessionId: {0}) error. ",
                    sessionId);
                return null;
            }
        }

        public async Task RemoveUserSessionByIdAsync(Guid sessionId)
        {
            try
            {
                await _cacheUserSession.RemoveAsync(sessionId.ToString("N"));
                var userSession = await _dbDbContext.UserSessions.FirstOrDefaultAsync(x => x.SessionId == sessionId);
                if (userSession != null)
                {
                    _dbDbContext.UserSessions.Remove(userSession);
                    await _dbDbContext.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Remove session for sql server and redis item (SessionId: {0}) error. ",
                    sessionId);
            }
        }

        public void RemoveUserSessionById(Guid sessionId)
        {
            try
            {
                _cacheUserSession.Remove(sessionId.ToString("N"));
                var userSession = _dbDbContext.UserSessions.FirstOrDefault(x => x.SessionId == sessionId);
                if (userSession != null)
                {
                    _dbDbContext.UserSessions.Remove(userSession);
                    _dbDbContext.SaveChanges();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Remove session for sql server and redis item (SessionId: {0}) error. ",
                    sessionId);
            }
        }

        #region Private Method

        private async Task<UserSession> GetSessionForRedisAsync(Guid sessionId)
        {
            var cacheUserSession = await _cacheUserSession.GetAsync(sessionId.ToString("N"));
            return cacheUserSession;
        }

        private UserSession GetSessionForRedis(Guid sessionId)
        {
            var cacheUserSession = _cacheUserSession.Get(sessionId.ToString("N"));
            return cacheUserSession;
        }

        #endregion
    }
}
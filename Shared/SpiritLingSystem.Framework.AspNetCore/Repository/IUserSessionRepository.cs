using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SpiritLingSystem.Framework.AspNetCore.Models;

namespace SpiritLingSystem.Framework.AspNetCore.Repository
{
    public interface IUserSessionRepository
    {
        /// <summary>
        /// 插入session到数据库和缓存中
        /// </summary>
        /// <param name="userSession"></param>
        /// <returns></returns>
        Task AddUserSessionAsync(UserSession userSession);
        
        /// <summary>
        /// 插入session到数据库和缓存中
        /// </summary>
        void AddUserSession(UserSession userSession);

        /// <summary>
        /// 查询usersession从缓存和数据库中
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        Task<UserSession> FindUserSessionByIdAsync(Guid sessionId);

        /// <summary>
        /// 查询usersession从缓存和数据库中
        /// </summary>
        UserSession FindUserSessionById(Guid sessionId);

        /// <summary>
        /// 删除usersession数据
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        Task RemoveUserSessionByIdAsync(Guid sessionId);

        /// <summary>
        /// 删除usersession数据
        /// </summary>
        void RemoveUserSessionById(Guid sessionId);
    
    }
}
﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using IdentityModel;
using IdentityServer4.Extensions;
using SpiritLingSystem.Library.Structures.Const.IdentityServer;

namespace SpiritLingSystem.Framework.AspNetCore.Extension
{
    public static class ClaimsPrincipalExtension
    {
        public static Guid? GetUserKey(this IIdentity identity)
        {
            var claims = identity as ClaimsIdentity;
            var str = claims?.Claims.First(c => c.Type == SpiritLingClaimTypes.UserKey).Value;
            if (Guid.TryParse(str, out Guid userKey))
            {
                return userKey;
            }
            return null;
        }
        
        public static Guid? GetSessionId(this IIdentity identity)
        {
            var claims = identity as ClaimsIdentity;
            var str =  claims?.Claims.FirstOrDefault(c => c.Type == SpiritLingClaimTypes.SessionId);
            if (str != null)
            {
                if (Guid.TryParse(str.Value, out Guid sessionId))
                {
                    return sessionId;
                }
            }
            return null;
        }
        
        public static string GetClientId(this IIdentity identity)
        {
            var claims = identity as ClaimsIdentity;
            var str = claims?.GetClientId();
            return str;
        }
        
        public static Guid GetUserKey(this ClaimsPrincipal claimsPrincipal)
        {
            var claims = claimsPrincipal;
            var useKeyStr = claims?.Claims.FirstOrDefault(c => c.Type == SpiritLingClaimTypes.UserKey);
            if (useKeyStr != null)
            {
                if (Guid.TryParse(useKeyStr.Value, out Guid userKey))
                {
                    return userKey;
                }
            }
            return Guid.Empty;
        }

        public static Guid? GetSessionId(this ClaimsPrincipal claimsPrincipal)
        {
            var claims = claimsPrincipal;
            var str = claims?.Claims.FirstOrDefault(c => c.Type == SpiritLingClaimTypes.SessionId);
            if (str != null)
            {
                if (Guid.TryParse(str.Value, out Guid sessionId))
                {
                    return sessionId;
                }
            }
            return null;
        }
        
        public static string GetClientId(this ClaimsPrincipal claimsPrincipal)
        {
            var claims = claimsPrincipal;
            var str = claims?.Claims.FirstOrDefault(c => c.Type == JwtClaimTypes.ClientId);
            if (str != null)
            {
                return str.Value;
            }
            return string.Empty;
        }
    }
}
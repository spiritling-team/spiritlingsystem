﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Configuration;
using Serilog.Events;
using Serilog.Formatting.Compact;
using Serilog.Sinks.SystemConsole.Themes;
using SpiritLingSystem.Library.Structures.Const.AppSettingConfigs;

namespace SpiritLingSystem.Framework.AspNetCore.Extension
{
    public static class LoggerConfigurationExtension
    {
        public static LoggerConfiguration AddEnrich(this LoggerConfiguration config, IConfiguration configuration)
        {
            return config.ReadFrom.Configuration(configuration)
                    .Enrich.FromLogContext()
                    .Enrich.WithClientAgent()
                    .Enrich.WithClientIp()
                //.Enrich.WithProperty("userInfo","")
                ;
            //.Enrich.WithProperty("userInfo","")
        }

        public static LoggerConfiguration AddDebug(this LoggerSinkConfiguration writeTo)
        {
            return writeTo.Debug();
        }

        public static LoggerConfiguration AddConsole(this LoggerSinkConfiguration writeTo)
        {
            return writeTo.Console(theme: AnsiConsoleTheme.Code,
                outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u}] {Message:lj}{NewLine}{Exception}",
                restrictedToMinimumLevel: LogEventLevel.Verbose);
        }

        /// <summary>
        /// 添加日志到文件中
        /// </summary>
        /// <param name="writeTo"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static LoggerConfiguration AddLog2File(this LoggerSinkConfiguration writeTo,
            AppSettingConfig appSettingConfig)
        {
            var serverName = appSettingConfig.SerilogSetting.ServerName;
            var errorConfiguration = appSettingConfig.SerilogSetting.ErrorConfig;
            var normalConfiguration = appSettingConfig.SerilogSetting.NormalConfig;
            var businessConfiguration = appSettingConfig.SerilogSetting.BusinessConfig;

            return writeTo.Logger(loggerConfig =>
                {
                    loggerConfig.Filter.ByIncludingOnly(le =>
                            le.Level == LogEventLevel.Warning || le.Level == LogEventLevel.Error ||
                            le.Level == LogEventLevel.Fatal)
                        .WriteTo.RollingFile(new CompactJsonFormatter(),
                            Path.Combine(errorConfiguration.Path, serverName + "_error.json"), LogEventLevel.Warning,
                            errorConfiguration.FileSizeLimitBytes, errorConfiguration.RetainedFileCountLimit);
                })
                .WriteTo.Logger(loggerConfig =>
                {
                    loggerConfig.Filter.ByIncludingOnly(le =>
                            le.Properties.TryGetValue("SourceContext", out var pValue) && pValue != null &&
                            pValue.ToString().Contains("Bussiness"))
                        .WriteTo.RollingFile(new CompactJsonFormatter(),
                            Path.Combine(businessConfiguration.Path, serverName + "_business.json"),
                            LogEventLevel.Information, businessConfiguration.FileSizeLimitBytes,
                            businessConfiguration.RetainedFileCountLimit);
                })
                .WriteTo.Logger(loggerConfig =>
                {
                    loggerConfig.Filter.ByIncludingOnly(le =>
                            le.Level == LogEventLevel.Information)
                        .WriteTo.RollingFile(new CompactJsonFormatter(),
                            Path.Combine(normalConfiguration.Path, serverName + "_normal.json"),
                            LogEventLevel.Information, normalConfiguration.FileSizeLimitBytes,
                            normalConfiguration.RetainedFileCountLimit);
                });
        }
    }
}
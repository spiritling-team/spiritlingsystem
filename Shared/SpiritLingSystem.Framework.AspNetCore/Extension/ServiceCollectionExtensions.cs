﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SpiritLingSystem.Framework.AspNetCore.Caching;
using SpiritLingSystem.Framework.AspNetCore.DbContexts;
using SpiritLingSystem.Framework.AspNetCore.Logger;
using SpiritLingSystem.Framework.AspNetCore.Repository;
using SpiritLingSystem.Library.Structures.Const.AppSettingConfigs;

namespace SpiritLingSystem.Framework.AspNetCore.Extension
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 添加自定义验证
        /// </summary>
        /// <param name="services"></param>
        /// <param name="authority"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomAuthentication(this IServiceCollection services, string authority)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.Authority = authority;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,//验证
                        ValidateAudience = false,//
                        ClockSkew = TimeSpan.FromSeconds(30),
                    };
                });
            return services;
        }

        /// <summary>
        /// 添加授权认证
        /// </summary>
        /// <param name="services"></param>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomAuthorization(this IServiceCollection services, List<string> scopes)
        {
            return services.AddAuthorization(options =>
            {
                options.AddPolicy("apiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", scopes);
                });
            });
        }
        
        /// <summary>
        /// Redis 缓存配置
        /// </summary>
        /// <param name="services"></param>
        /// <param name="redisConfig"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomRedisCache(this IServiceCollection services, string redisConfig)
        {
            return services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = redisConfig;
            });
        }
        
        /// <summary>
        /// API 添加自定义swagger
        /// </summary>
        /// <param name="services"></param>
        /// <param name="nameSpace"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomSwaggerGen(this IServiceCollection services,string nameSpace)
        {
            return services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = $"SpiritLingSystem.{nameSpace}.API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
            });
        }
        
        public static IServiceCollection AddTryCore(this IServiceCollection services)
        {
            services.TryAddTransient(typeof(IBusinessLogger<>), typeof(BusinessLogger<>));
            services.TryAddTransient<ISystemSession,SystemSession>();
            services.TryAddTransient(typeof(IDistributedCache<>),typeof(DistributedCache<>));
            return services;
        }

        /// <summary>
        /// 添加认证session等操作
        /// </summary>
        /// <param name="services"></param>
        /// <param name="appSetting"></param>
        /// <returns></returns>
        public static IServiceCollection AddUserSession(this IServiceCollection services,AppSettingConfig appSetting)
        {
            services.AddDbContext<SharedIdsvDbContext>(options =>
                options.UseSqlServer(appSetting.DataBaseSetting.IdentityServer));
            services.TryAddTransient(typeof(IDistributedCache<>),typeof(DistributedCache<>));
            services.TryAddTransient<IUserSessionRepository,UserSessionRepository>();
            return services;
        }
    }
}
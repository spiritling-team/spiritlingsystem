﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.Framework.AspNetCore.Models
{
    public partial class UserSession
    {
        public Guid SessionId { get; set; }
        public Guid UserKey { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime Expiration { get; set; }
    }
}

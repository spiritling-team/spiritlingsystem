﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace SpiritLingSystem.Framework.AspNetCore.Common
{
    public static class Utils
    {
        public static string GetIpAddress(HttpContextAccessor  httpContextAccessor)
        {
            var ipAddress = httpContextAccessor.HttpContext?.Request.Headers["X-forwarded-for"].FirstOrDefault();

            if (!string.IsNullOrEmpty(ipAddress))
            {
                return GetIpAddressFromProxy(ipAddress);
            }

            return httpContextAccessor.HttpContext?.Connection.RemoteIpAddress?.MapToIPv4().ToString();
        }
        
        private static string GetIpAddressFromProxy(string proxiedIpList)
        {
            var addresses = proxiedIpList.Split(',');

            if (addresses.Length != 0)
            {
                // If IP contains port, it will be after the last : (IPv6 uses : as delimiter and could have more of them)
                return addresses[0].Contains(":")
                    ? addresses[0].Substring(0, addresses[0].LastIndexOf(":", StringComparison.Ordinal))
                    : addresses[0];
            }

            return string.Empty;
        }
    }
}
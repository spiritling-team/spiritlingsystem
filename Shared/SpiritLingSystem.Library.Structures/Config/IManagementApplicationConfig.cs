﻿using System.Threading.Tasks;

namespace SpiritLingSystem.Library.Structures.Config
{
    public enum ConfigType
    {
        Number = 0,
        String =1
    }
    public interface IManagementApplicationConfig
    {
        /// <summary>
        /// 异步获取ApplicationConfig的值
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<string> GetConfigValueAsync(string name);
        string GetConfigValue(string name);

        Task<int> GetConfigIntValueAsync(string name);
        
        int GetConfigIntValue(string name);
    }
}
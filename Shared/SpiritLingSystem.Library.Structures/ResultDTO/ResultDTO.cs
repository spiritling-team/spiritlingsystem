﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SpiritLingSystem.Library.Structures.ResultDTO
{
    /// <summary>
    /// 因为Data是个泛型，所以在某些序列化是无法知道具体值，但是只需要除data外的其他值，所以使用这个原始值来反序列化
    /// </summary>
    public partial class ResultPrototype : IResultPrototype
    {
        public ResultPrototype() { }

        public ResultPrototype(long code = 0, string msgKey = "", string message = "", Dictionary<string, List<string>> errorList = default)
        {
            Code = code;
            MsgKey = msgKey;
            Message = message;
            Success = (code < 400);
            ErrorList = errorList;
        }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("msgKey")]
        public string MsgKey { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("errorList")]
        public Dictionary<string, List<string>> ErrorList { get; set; }

    }

    public partial class ResultDTO<T> : IResultDTO<T>
    {
        public ResultDTO() { }

        public ResultDTO(T data = default, long code = 200, string msgKey = "OK", string message = "The request has succeeded.", Dictionary<string, List<string>> errorList = default)
        {
            Data = data;
            Code = code;
            MsgKey = msgKey;
            Message = message;
            Success = (code < 400);
            ErrorList = errorList;
        }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }

        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("msgKey")]
        public string MsgKey { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("errorList")]
        public Dictionary<string, List<string>> ErrorList { get; set; }


        /// <summary>
        /// 200 OK
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> OkResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The request has succeeded.",
                MsgKey = "OK",
                Code = 200,
            };
        }

        /// <summary>
        /// 301 Moved Permanently
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> MovedPermanentlyResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The target resource has been assigned a new permanent URI and any future references to this resource ought to use one of the enclosed URIs.",
                MsgKey = "MOVED PERMANENTLY",
                Code = 301,
            };
        }

        /// <summary>
        /// 302 Found
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> FoundResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The target resource resides temporarily under a different URI. Since the redirection might be altered on occasion, the client ought to continue to use the effective request URI for future requests.",
                MsgKey = "Found",
                Code = 302,
            };
        }

        /// <summary>
        /// 400 Bad Request
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> BadRequestResult(string message = null, string msgKey = null)
        {
            return new ResultDTO<T>()
            {
                Message = message ?? "The server cannot or will not process the request due to something that is perceived to be a client error.",
                MsgKey = msgKey ?? "Bad Request",
                Code = 400,
            };
        }

        /// <summary>
        /// 401 Unauthorized
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> UnauthorizedResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The request has not been applied because it lacks valid authentication credentials for the target resource.",
                MsgKey = "Unauthorized",
                Code = 401,
            };
        }

        /// <summary>
        /// 403 Forbidden
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> ForbiddenResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The server understood the request but refuses to authorize it.",
                MsgKey = "Forbidden",
                Code = 403,
            };
        }

        /// <summary>
        /// 404 Not Found
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> NotFoundResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The origin server did not find a current representation for the target resource or is not willing to disclose that one exists.",
                MsgKey = "Not Found",
                Code = 404,
            };
        }

        /// <summary>
        /// 405 Method Not Allowed
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> MethodNotAllowedResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The method received in the request-line is known by the origin server but not supported by the target resource.",
                MsgKey = "Method Not Allowed",
                Code = 405,
            };
        }

        /// <summary>
        /// 500 Internal Server Error
        /// </summary>
        /// <returns></returns>
        public static ResultDTO<T> InternalServerErrorResult()
        {
            return new ResultDTO<T>()
            {
                Message = "The server encountered an unexpected condition that prevented it from fulfilling the request.",
                MsgKey = "Internal Server Error",
                Code = 500,
            };
        }

    }
}
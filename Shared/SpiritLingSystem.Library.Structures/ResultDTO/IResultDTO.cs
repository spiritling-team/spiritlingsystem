﻿using System.Collections.Generic;

namespace SpiritLingSystem.Library.Structures.ResultDTO
{
    public interface IResultPrototype {
        bool Success { get; set; }
        long Code { get; set; }
        string MsgKey { get; set; }
        string Message { get; set; }

        Dictionary<string,List<string>> ErrorList { get; set; }
    }

    public interface IResultDTO<T>: IResultPrototype
    {
        T Data { get; set; }
    }
}
namespace SpiritLingSystem.Library.Structures.Const.IdentityServer
{
    public enum SpiritLingOpenIDProvider
    {
        Github = 1,
        Google,
        Coding,
        Office,
        Wechat,
    }
}
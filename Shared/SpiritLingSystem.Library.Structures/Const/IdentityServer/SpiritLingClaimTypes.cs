﻿namespace SpiritLingSystem.Library.Structures.Const.IdentityServer
{
    public static class SpiritLingClaimTypes
    {
        /// <summary>
        /// 用户唯一Key
        /// </summary>
        public const string SessionId = "sessionid";
        public const string UserKey = "userkey";
        public const string Avatar = "avatar";
        public const string Status = "status";
        public const string CreatedTime = "created_at";
        public const string UpdatedTime = "updated_at";
    }
}
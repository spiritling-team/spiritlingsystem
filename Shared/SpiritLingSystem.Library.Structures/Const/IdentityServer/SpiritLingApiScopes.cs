﻿namespace SpiritLingSystem.Library.Structures.Const.IdentityServer
{
    public static class SpiritLingApiScopes
    {
        public const string OpenId = "openid";
        public const string Profile = "profile";
        public const string MangerApi = "ManageApi";
    }
}
﻿namespace SpiritLingSystem.Library.Structures.Const.IdentityServer
{
    public static class SpiritLingIdentityConfig
    {
        public const string ClientId = "SpiritLingSystemClientId_HP1TEE08U24U0WWM";
        public const string Secret = "SpiritLingSecret_MCWHUETDM441H2R8";
        public const string Scope = "SpiritLingScope_CUWR482H1HMP200Y";
        public const string ScopeDisplay = "SpiritLingScopeDisplay_0K58148WRCWM0Y84";
    }
}
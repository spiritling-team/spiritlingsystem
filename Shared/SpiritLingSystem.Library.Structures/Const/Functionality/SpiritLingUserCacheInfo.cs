using System.Collections.Generic;

namespace SpiritLingSystem.Library.Structures.Const.Functionality
{
    public class SpiritLingUserCacheInfo
    {
        public List<int> Functionalities { get; set; }
    }
}
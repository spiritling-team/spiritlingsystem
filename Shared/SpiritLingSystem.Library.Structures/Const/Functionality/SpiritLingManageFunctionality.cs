namespace SpiritLingSystem.Library.Structures.Const.Functionality
{
    /// <summary>
    /// 管理服务权限
    /// </summary>
    public class SpiritLingManageFunctionality
    {
        /// <summary>
        /// 客户端 - 子集需要最顶层这个，否则无法使用
        /// </summary>
        public const int Client = 101000;

        /// <summary>
        /// 客户端·一览
        /// </summary>
        public const int ClientOverview = 101001;

        /// <summary>
        /// 客户端·详细
        /// </summary>
        public const int ClientDetail = 101002;

        /// <summary>
        /// 客户端·编辑
        /// </summary>
        public const int ClientEdit = 101003;

        /// <summary>
        /// 客户端范围·一览
        /// </summary>
        public const int ClientScope = 101004;
    }
}
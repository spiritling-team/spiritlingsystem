namespace SpiritLingSystem.Library.Structures.Const.Functionality
{
    /// <summary>
    /// 服务
    /// </summary>
    public static class SpiritLingServices
    {
        /// <summary>
        /// 后台管理服务
        /// </summary>
        public const int ManageService = 100000;
    }
}
﻿using System;
using System.Numerics;

namespace SpiritLingSystem.Library.Structures.Const.Coding
{
    public class CodingCurrentUserResponse
    {
        public Guid RequestId { get; set; }
        public CodingUser User { get; set; }
    }

    public class CodingUser
    {
        /// <summary>
        /// 唯一ID
        /// </summary>
        public int Id { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
    }
}
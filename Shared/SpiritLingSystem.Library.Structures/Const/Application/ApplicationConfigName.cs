﻿namespace SpiritLingSystem.Library.Structures.Const.Application
{
    public static class ApplicationConfigName
    {
        #region Third Party OAuth

        public const string CodingClientID = "CodingClientID";
        public const string CodingClientSecret = "CodingClientSecret";
        public const string GithubClientID = "GithubClientID";
        public const string GithubClientSecret = "GithubClientSecret";

        #endregion

        #region Captcha

        public const string TencentCaptchaID = "TencentCaptchaID";
        public const string TencentCaptchaSecret = "TencentCaptchaSecret";

        #endregion
        
        #region IdentityServer

        public const string CookieSessionExpires = "IdentityServerCookieSessionExpires";

        #endregion

        #region Host

        public const string CodingApiHost = "CodingApiHost";

        #endregion
    }
}
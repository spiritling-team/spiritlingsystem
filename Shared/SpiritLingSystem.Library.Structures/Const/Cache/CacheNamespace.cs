﻿namespace SpiritLingSystem.Library.Structures.Const.Cache
{
    /// <summary>
    /// 缓存命名空间，自带 <b>:</b>
    /// </summary>
    public static class CacheNamespace
    {
        public const string SystemManagement = "SystemManagement:";
    }
}
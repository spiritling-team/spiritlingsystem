﻿namespace SpiritLingSystem.Library.Structures.Const.Cache
{
    public static class CacheKey
    {
        public const string ApplicationConfigCacheKey = "APPLICATION_CONFIG_CACHE";
    }
}
﻿namespace SpiritLingSystem.Library.Structures.Const.AppSettingConfigs
{
    public class DataBaseSettingConfig
    {
        /// <summary>
        /// IdentityServer 认证授权数据库连接字符串
        /// </summary>
        public string IdentityServer { get; set; }
        /// <summary>
        /// Redis 数据库配置
        /// </summary>
        public string Redis { get; set; }
        /// <summary>
        /// 博客站点数据库
        /// </summary>
        public string BlogSite { get; set; }
        
        /// <summary>
        /// 管理核心库
        /// </summary>
        public string Management { get; set; }
    }
}
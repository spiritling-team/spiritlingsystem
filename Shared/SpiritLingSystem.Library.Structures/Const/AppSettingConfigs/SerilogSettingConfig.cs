﻿namespace SpiritLingSystem.Library.Structures.Const.AppSettingConfigs
{
    public class SerilogSettingConfig
    {
        public string ServerName { get; set; } = "SpiritLingSystem";
        /// <summary>
        /// 严重错误日志配置
        /// </summary>
        public SerilogSettingConifgCommon ErrorConfig { get; set; } = new SerilogSettingConifgCommon();
        
        /*
         * _logger.LogTrace("Trace"); // Verbose
            _logger.LogDebug("Debug");
            _logger.LogInformation("Information");
            _logger.LogWarning("Warning");
            _logger.LogError("Error");
            _logger.LogCritical("Critical"); // == Fatal
         */
        /// <summary>
        /// 信息日志配置
        /// </summary>
        public SerilogSettingConifgCommon NormalConfig { get; set; } = new SerilogSettingConifgCommon();

        public SerilogSettingConifgCommon BusinessConfig { get; set; } = new SerilogSettingConifgCommon();
    }
    
    
    /// <summary>
    /// serilog日志共通
    /// </summary>
    public class SerilogSettingConifgCommon
    {
        /// <summary>
        /// 存放日志路径 默认：logs/error
        /// </summary>
        public string Path { get; set; } = "logs";

        /// <summary>
        /// 文件大小 20971520 = 20M
        /// </summary>
        public int FileSizeLimitBytes { get; set; } = 20971520;

        /// <summary>
        /// 存放文件数量:200
        /// </summary>
        public int RetainedFileCountLimit { get; set; } = 200;
    }
    
}
﻿using System.Collections.Generic;

namespace SpiritLingSystem.Library.Structures.Const.AppSettingConfigs
{
    
    /// <summary>
    /// 统一的appsetting配置接口
    /// </summary>
    public partial class AppSettingConfig
    {
        /// <summary>
        /// Serilog 日志配置
        /// </summary>
        public SerilogSettingConfig SerilogSetting { get; set; }
        /// <summary>
        /// 数据库配置
        /// </summary>
        public DataBaseSettingConfig DataBaseSetting { get; set; }

        /// <summary>
        /// 环境配置key - 开发环境一般为DEBUG 生产环境一般为 PROD
        /// </summary>
        public string ConfigKey { get; set; } = "PROD";
    }
}
using System;
using System.ComponentModel;
using SpiritLingSystem.Library.Tools.AttributeExtension;
using SpiritLingSystem.Library.Tools.EnumExtension;
using Xunit;

namespace Library.Tools.Test.EnumExtensionTest
{
    public class EnumGetTextExtensionTest
    {
        [Fact]
        public void EnumGetDescription()
        {
            var descText = EnumDescriptionTest.EnumDescriptionTestText.GetDescription();
            
            Assert.Equal(descText,EnumDescriptionText.EnumDescriptionTestText);
        }
        
        [Fact]
        public void EnumGetInformation()
        {
            var infoText = EnumDescriptionTest.EnumInformationTestText.GetInformation();
            
            Assert.Equal(infoText,EnumDescriptionText.EnumInformationTestText);
        }
    }

    public enum EnumDescriptionTest
    {
        [Description(EnumDescriptionText.EnumDescriptionTestText)]
        EnumDescriptionTestText,
        [Information(EnumDescriptionText.EnumInformationTestText)]
        EnumInformationTestText
    }

    public static class EnumDescriptionText
    {
        public const string EnumDescriptionTestText = "EnumDescriptionTestText";
        public const string EnumInformationTestText = "EnumInformationTestText";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using SpiritLingSystem.Library.Tools.Common;

namespace SpiritLingSystem.Library.Tools.EncryptionExtension
{
    public static class EncryptionMD5
    {
        /// <summary>
        /// 普通Md5加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Md5Encryption(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            return Md5Core(str);
        }
        /// <summary>
        /// Md5加密加盐
        /// </summary>
        /// <param name="str"></param>
        /// <param name="salt">盐</param>
        /// <returns></returns>
        public static string Md5Encryption(this string str,string salt)
        {
            if (CommonString.AnyNullOrWhiteSpace(str, salt))
            {
                return string.Empty;
            }
            return Md5Core(str + salt);
        }
        /// <summary>
        /// 使用模板进行MD5加密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="template">Example: spirit{0}ling</param>
        /// <returns></returns>
        public static string Md5EncryptionWithTemplate(this string str, string template)
        {
            if (CommonString.AnyNullOrWhiteSpace(str, template))
            {
                return string.Empty;
            }
            return Md5Core(string.Format(template,str));
        }

        /// <summary>
        /// 使用模板进行MD5加密加盐
        /// </summary>
        /// <param name="str"></param>
        /// <param name="salt">盐</param>
        /// <param name="template">Example: spirit{0}ling{1}</param>
        /// <returns></returns>
        public static string Md5EncryptionWithTemplate(this string str, string template, string salt)
        {
            if (CommonString.AnyNullOrWhiteSpace(str,salt,template))
            {
                return string.Empty;
            }
            return Md5Core(string.Format(template, str,salt));
        }

        private static string Md5Core(string str)
        {
            MD5 md5 = MD5.Create();
            // 将字符串转换成字节数组
            byte[] byteOld = Encoding.UTF8.GetBytes(str);
            // 调用加密方法
            byte[] byteNew = md5.ComputeHash(byteOld);
            // 将加密结果转换为字符串
            StringBuilder sb = new StringBuilder();
            foreach (byte b in byteNew)
            {
                // 将字节转换成16进制表示的字符串，
                sb.Append(b.ToString("x2"));
            }
            // 返回加密的字符串
            return sb.ToString();
        }
    }
}

﻿using SpiritLingSystem.Library.Tools.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.Library.Tools.EncryptionExtension
{
    public static class EncryptionSHA256
    {
        /// <summary>
        /// 普通SHA256加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SHA256Encryption(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return string.Empty;
            }
            return SHA256Core(str);
        }
        /// <summary>
        /// SHA256加密加盐
        /// </summary>
        /// <param name="str"></param>
        /// <param name="salt">盐</param>
        /// <returns></returns>
        public static string SHA256Encryption(this string str, string salt)
        {
            if (CommonString.AnyNullOrWhiteSpace(str, salt))
            {
                return string.Empty;
            }
            return SHA256Core(str + salt);
        }
        /// <summary>
        /// 使用模板进行SHA256加密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="template">Example: spirit{0}ling</param>
        /// <returns></returns>
        public static string SHA256EncryptionWithTemplate(this string str, string template)
        {
            if (CommonString.AnyNullOrWhiteSpace(str, template))
            {
                return string.Empty;
            }
            return SHA256Core(string.Format(template, str));
        }

        /// <summary>
        /// 使用模板进行SHA256加密加盐
        /// </summary>
        /// <param name="str"></param>
        /// <param name="salt">盐</param>
        /// <param name="template">Example: spirit{0}ling{1}</param>
        /// <returns></returns>
        public static string SHA256EncryptionWithTemplate(this string str, string template, string salt)
        {
            if (CommonString.AnyNullOrWhiteSpace(str, salt, template))
            {
                return string.Empty;
            }
            return SHA256Core(string.Format(template, str, salt));
        }

        private static string SHA256Core(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);
            using (var hash = SHA256.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 256 bits / 8 bits in byte * 2 symbols for byte 
                var hashedInputStringBuilder = new StringBuilder(64);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                return hashedInputStringBuilder.ToString();
            }
        }
    }
}

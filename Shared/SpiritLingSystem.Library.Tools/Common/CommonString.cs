﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.Library.Tools.Common
{
    public static class CommonString
    {
        public static bool AnyNullOrWhiteSpace(params string[] strs)
        {
            bool result = false;
            for (int i = 0; i < strs.Length; i++)
            {
                result = result || string.IsNullOrWhiteSpace(strs[i]);
            }
            return result;
        }
    }
}

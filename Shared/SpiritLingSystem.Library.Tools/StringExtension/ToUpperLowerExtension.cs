﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.Library.Tools.StringExtension
{
    public static class ToUpperLowerExtension
    {
        /// <summary>
        /// 首字母大写
        /// </summary>
        public static string ToUpperFirstLetter(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            char[] letters = value.ToCharArray();
            letters[0] = char.ToUpper(letters[0]);

            return new string(letters);
        }
        /// <summary>
        /// 首字母小写
        /// </summary>
        public static string ToLowerFirstLetter(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            char[] letters = value.ToCharArray();
            letters[0] = char.ToLower(letters[0]);

            return new string(letters);
        }
        /// <summary>
        /// 字符串转大写
        /// </summary>
        public static string ToUpper(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            char[] letters = value.ToCharArray();
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i] = char.ToUpper(letters[i]);
            }

            return new string(letters);
        }
        /// <summary>
        /// 字符串转小写
        /// </summary>
        public static string ToLower(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            char[] letters = value.ToCharArray();
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i] = char.ToLower(letters[i]);
            }

            return new string(letters);
        }
    }
}

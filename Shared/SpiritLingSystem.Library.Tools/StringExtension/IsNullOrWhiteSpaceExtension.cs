﻿namespace SpiritLingSystem.Library.Tools.StringExtension
{
    public static class IsNullOrWhiteSpaceExtension
    {
        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }
    }
}
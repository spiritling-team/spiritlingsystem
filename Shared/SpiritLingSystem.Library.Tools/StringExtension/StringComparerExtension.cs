﻿using System;

namespace SpiritLingSystem.Library.Tools.StringExtension
{
    public static class StringComparerExtension
    {
        public static bool EqualsIgnoreCase(this string str, string target)
        {
            if (str.IsNullOrWhiteSpace() || target.IsNullOrWhiteSpace())
            {
                return false;
            }

            return str.ToUpper() == target.ToUpper();
        }
    }
}
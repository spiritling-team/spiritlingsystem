﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpiritLingSystem.Library.Tools.EncryptionExtension;

namespace SpiritLingSystem.Library.Tools.StringExtension
{
    public static class PasswordExtension
    {
        public static string EncryptUser(this string str, string guid)
        {
            return str.SHA512EncryptionWithTemplate("spirit{0}ling{1}",guid);
        }
    }
}

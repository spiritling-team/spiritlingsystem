﻿using SpiritLingSystem.Library.Tools.Consts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.Library.Tools.RandomExtension
{
    public static class RandomStringExtension
    {
        /// <summary>
        /// 返回一个位数在0-32位的字符串
        /// </summary>
        /// <param name="random"></param>
        /// <returns></returns>
        public static string NextString(this Random random)
        {
            var len = random.Next();
            return NextStringRun(len);
        }

        /// <summary>
        /// 返回一个指定位数的字符串
        /// </summary>
        /// <param name="random"></param>
        /// <param name="lens">字符串位数</param>
        /// <returns></returns>
        public static string NextString(this Random random, int lens)
        {
            var len = random.Next(lens, lens);
            return NextStringRun(len);
        }

        /// <summary>
        /// 返回一个自定义字符库的字符串
        /// </summary>
        /// <param name="random"></param>
        /// <param name="lens">字符串长度</param>
        /// <param name="customeString">自定义字符库</param>
        /// <returns></returns>
        public static string NextString(this Random random, int lens, string customeString)
        {
            var len = random.Next(lens, lens);
            return NextStringRun(len, customeString);
        }


        private static string NextStringRun(int len, string randomStr = null)
        {
            var chars = (string.IsNullOrWhiteSpace(randomStr) ? StringConsts.RandomStringDefault : randomStr.Trim()).ToCharArray();
            var str = new StringBuilder(len);
            while (len > 0)
            {
                var r = new Random();
                str.Append(chars[r.Next(chars.Length - 1)]);
                len--;
            }
            return str.ToString();
        }
    }
}

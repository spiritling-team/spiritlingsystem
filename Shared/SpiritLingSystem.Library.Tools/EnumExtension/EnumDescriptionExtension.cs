﻿using SpiritLingSystem.Library.Tools.AttributeExtension;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.Library.Tools.EnumExtension
{
    /// <summary>
    /// Enum Description 静态扩展方法
    /// </summary>
    public static class EnumGetTextExtension
    {
        /// <summary>
        /// 获取枚举值上的Description特性的说明
        /// </summary>
        /// <returns>特性的说明</returns>
        public static string GetDescription(this Enum en)
        {
            var type = en.GetType();
            FieldInfo field = type.GetField(Enum.GetName(type, en));
            if (!(Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute descAttr))
            {
                return string.Empty;
            }

            return descAttr.Description;
        }

        /// <summary>
        /// 获取枚举值上的Information特性的说明
        /// </summary>
        /// <returns>特性的说明</returns>
        public static string GetInformation(this Enum en)
        {
            var type = en.GetType();
            FieldInfo field = type.GetField(Enum.GetName(type, en));
            if (!(Attribute.GetCustomAttribute(field, typeof(InformationAttribute)) is InformationAttribute descAttr))
            {
                return string.Empty;
            }

            return descAttr.Information;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.Library.Tools.AttributeExtension
{
    [AttributeUsage(AttributeTargets.All)]
    public class InformationAttribute:Attribute
    {
        public static readonly InformationAttribute Default = new InformationAttribute();
        private string information;
        public InformationAttribute() : this(string.Empty)
        {
        }
        public InformationAttribute(string information) => this.information = information;

        public virtual string Information => this.InformationValue;

        protected string InformationValue
        {
            get => this.information;
            set => this.information = value;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;
            return obj is InformationAttribute informationAttribute && informationAttribute.Information == this.Information;
        }

        public override int GetHashCode() => this.Information.GetHashCode();
        public override bool IsDefaultAttribute() => this.Equals((object)InformationAttribute.Default);
    }
}

using System;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace SpiritLingSystem.Library.Tools.Authentication
{
    public static class OtpAuth
    {
        const int IntervalLength = 30;
        const int PinLength = 6;
        static readonly int PinModulo = (int) Math.Pow(10, PinLength);
        static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        ///   Number of intervals that have elapsed.
        /// </summary>
        static long CurrentInterval
        {
            get
            {
                var elapsedSeconds = (long) Math.Floor((DateTime.UtcNow - UnixEpoch).TotalSeconds);

                return elapsedSeconds / IntervalLength;
            }
        }

        
        /// <summary>
        /// Generates a opt auth url
        /// </summary>
        /// <param name="identifier">用户标识一般邮箱手机号</param>
        /// <param name="key">唯一的key</param>
        /// <param name="issuer">表示站点等</param>
        /// <returns>otpauth://totp/{issuer}:{identifier}?secret=secret({key})&issuer={issuer}</returns>
        public static string GenerateOptAuth(string identifier, byte[] key, string issuer = "SpiritLing")
        {
            var keyString = Encoder.Base32Encode(key);
            // Google: otpauth://totp/GitHub:xxxx?secret=isqkvxnmnlb3ce7v&issuer=GitHub2
            // Google 验证器 显示为 GitHub2(GitHub:xxxx)
            // Microsoft: otpauth://totp/GitHub:xxxx?secret=isqkvxnmnlb3ce7v&issuer=GitHub2
            // Microsoft显示为 GitHub(xxxx)
            // 兼容起见包含issuer 和前面一致
            var provisionUrl = $"otpauth://totp/{issuer}:{identifier}?secret={keyString}&issuer={issuer}";

            // 通过生成google接口生产二维码
            //var ChartUrl = string.Format("https://chart.apis.google.com/chart?cht=qr&chs={0}x{1}&chl={2}", width, height, ProvisionUrl);
            //using (var Client = new WebClient())
            //{
            //    return Client.DownloadData(ChartUrl);
            //}
            return provisionUrl;
        }

        /// <summary>
        /// Generates a opt auth url
        /// </summary>
        /// <param name="identifier">用户标识一般邮箱手机号</param>
        /// <param name="key">唯一的key</param>
        /// <param name="issuer">表示站点等</param>
        /// <returns>otpauth://totp/{issuer}:{identifier}?secret=secret({key})&issuer={issuer}</returns>
        public static string GenerateOptAuth(string identifier, string key, string issuer = "SpiritLing")
        {
            return GenerateOptAuth(identifier, Encoding.UTF8.GetBytes(key), issuer);
        }

        /// <summary>
        ///   Generates a pin for the given key.
        /// </summary>
        public static string GeneratePin(byte[] key)
        {
            return GeneratePin(key, CurrentInterval);
        }

        public static string GeneratePin(string key)
        {
            return GeneratePin(Encoding.UTF8.GetBytes(key));
        }

        /// <summary>
        ///   Generates a pin by hashing a key and counter.
        /// </summary>
        static string GeneratePin(byte[] key, long counter)
        {
            const int sizeOfInt32 = 4;

            var counterBytes = BitConverter.GetBytes(counter);

            if (BitConverter.IsLittleEndian)
            {
                //spec requires bytes in big-endian order
                Array.Reverse(counterBytes);
            }

            var hash = new HMACSHA1(key).ComputeHash(counterBytes);
            var offset = hash[hash.Length - 1] & 0xF;

            var selectedBytes = new byte[sizeOfInt32];
            Buffer.BlockCopy(hash, offset, selectedBytes, 0, sizeOfInt32);

            if (BitConverter.IsLittleEndian)
            {
                //spec interprets bytes in big-endian order
                Array.Reverse(selectedBytes);
            }

            var selectedInteger = BitConverter.ToInt32(selectedBytes, 0);

            //remove the most significant bit for interoperability per spec
            var truncatedHash = selectedInteger & 0x7FFFFFFF;

            //generate number of digits for given pin length
            var pin = truncatedHash % PinModulo;

            return pin.ToString(CultureInfo.InvariantCulture).PadLeft(PinLength, '0');
        }

        #region Nested type: Encoder

        static class Encoder
        {
            /// <summary>
            ///   Url Encoding (with upper-case hexadecimal per OATH specification)
            /// </summary>
            public static string UrlEncode(string value)
            {
                const string urlEncodeAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";

                var builder = new StringBuilder();

                foreach (var symbol in value)
                {
                    if (urlEncodeAlphabet.IndexOf(symbol) != -1)
                    {
                        builder.Append(symbol);
                    }
                    else
                    {
                        builder.Append('%');
                        builder.Append(((int) symbol).ToString("X2"));
                    }
                }

                return builder.ToString();
            }

            /// <summary>
            ///   Base-32 Encoding
            /// </summary>
            public static string Base32Encode(byte[] data)
            {
                const int inByteSize = 8;
                const int outByteSize = 5;
                const string base32Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

                int i = 0, index = 0;
                var builder = new StringBuilder((data.Length + 7) * inByteSize / outByteSize);

                while (i < data.Length)
                {
                    int currentByte = data[i];
                    int digit;

                    //Is the current digit going to span a byte boundary?
                    if (index > (inByteSize - outByteSize))
                    {
                        int nextByte;

                        if ((i + 1) < data.Length)
                        {
                            nextByte = data[i + 1];
                        }
                        else
                        {
                            nextByte = 0;
                        }

                        digit = currentByte & (0xFF >> index);
                        index = (index + outByteSize) % inByteSize;
                        digit <<= index;
                        digit |= nextByte >> (inByteSize - index);
                        i++;
                    }
                    else
                    {
                        digit = (currentByte >> (inByteSize - (index + outByteSize))) & 0x1F;
                        index = (index + outByteSize) % inByteSize;

                        if (index == 0)
                        {
                            i++;
                        }
                    }

                    builder.Append(base32Alphabet[digit]);
                }

                return builder.ToString();
            }
        }

        #endregion
    }
}
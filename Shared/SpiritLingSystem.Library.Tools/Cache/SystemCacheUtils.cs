﻿using System;
using SpiritLingSystem.Library.Structures.Const.Cache;

namespace SpiritLingSystem.Library.Tools.Cache
{
    public static class SystemCacheUtils
    {
        /// <summary>
        /// 获取ApplicationConfig在Redis中缓存的Key值
        /// </summary>
        /// <param name="configKey">环境配置Key值：DEBUG PROD</param>
        /// <param name="keyName">需要存放的Key值名称</param>
        /// <returns></returns>
        public static string ApplicationConfigNormalKey(string configKey,string keyName)
        {
            return  CacheNamespace.SystemManagement + CacheKey.ApplicationConfigCacheKey + "_" + configKey + "_" + keyName;
        }

        /// <summary>
        /// 使用自定义扩展 <see>DistributedCache<TCacheItem></see> 不需要进行添加namespace
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="userKey"></param>
        /// <returns></returns>
        public static string UserClientPermission(string clientId,Guid userKey)
        {
            return clientId + "_" + userKey.ToString("N");
        }
    }
}
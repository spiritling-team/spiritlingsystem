﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class ArticleContent
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public string Markdown { get; set; }
        public string Html { get; set; }
        public DateTime CreatedTime { get; set; }
        public string Commit { get; set; }
        public string Uid { get; set; }

        public virtual Article Article { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class Author
    {
        public Author()
        {
            ArticleAuthorMaps = new HashSet<ArticleAuthorMap>();
        }

        public int Id { get; set; }
        public string Uid { get; set; }
        public string DisplayName { get; set; }
        public string Url { get; set; }
        public string Memo { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual ICollection<ArticleAuthorMap> ArticleAuthorMaps { get; set; }
    }
}

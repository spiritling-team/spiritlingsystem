﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class Tag
    {
        public Tag()
        {
            ArticleTagMaps = new HashSet<ArticleTagMap>();
        }

        public int Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public string BgColor { get; set; }
        public string TextColor { get; set; }
        public string Memo { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string Url { get; set; }

        public virtual ICollection<ArticleTagMap> ArticleTagMaps { get; set; }
    }
}

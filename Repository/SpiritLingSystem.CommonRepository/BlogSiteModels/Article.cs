﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class Article
    {
        public Article()
        {
            ArticleAuthorMaps = new HashSet<ArticleAuthorMap>();
            ArticleClassifyMaps = new HashSet<ArticleClassifyMap>();
            ArticleContents = new HashSet<ArticleContent>();
            ArticleSharePasswords = new HashSet<ArticleSharePassword>();
            ArticleTagMaps = new HashSet<ArticleTagMap>();
        }

        public int Id { get; set; }
        public string Uid { get; set; }
        public string DisplayName { get; set; }
        public bool IsPublic { get; set; }
        public bool IsSharePwd { get; set; }
        public DateTime? PublicDate { get; set; }
        public DateTime PublishStartDate { get; set; }
        public DateTime PublishEndDdate { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<ArticleAuthorMap> ArticleAuthorMaps { get; set; }
        public virtual ICollection<ArticleClassifyMap> ArticleClassifyMaps { get; set; }
        public virtual ICollection<ArticleContent> ArticleContents { get; set; }
        public virtual ICollection<ArticleSharePassword> ArticleSharePasswords { get; set; }
        public virtual ICollection<ArticleTagMap> ArticleTagMaps { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class Sentence
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class ArticleSharePassword
    {
        public ArticleSharePassword()
        {
            ArticleSharePasswordHistories = new HashSet<ArticleSharePasswordHistory>();
        }

        public int Id { get; set; }
        public int ArticleId { get; set; }
        public string Password { get; set; }
        public DateTime? ExpireTime { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual Article Article { get; set; }
        public virtual ICollection<ArticleSharePasswordHistory> ArticleSharePasswordHistories { get; set; }
    }
}

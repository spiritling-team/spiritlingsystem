﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class ArticleSharePasswordHistory
    {
        public int Id { get; set; }
        public int PasswordId { get; set; }
        public string Ip { get; set; }
        public string BrowserInfo { get; set; }
        public string BrowserFingerprint { get; set; }
        public DateTime CreatedTime { get; set; }

        public virtual ArticleSharePassword Password { get; set; }
    }
}

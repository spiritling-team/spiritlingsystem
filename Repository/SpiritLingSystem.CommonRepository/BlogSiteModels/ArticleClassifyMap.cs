﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class ArticleClassifyMap
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int ClassifyId { get; set; }

        public virtual Article Article { get; set; }
        public virtual Classify Classify { get; set; }
    }
}

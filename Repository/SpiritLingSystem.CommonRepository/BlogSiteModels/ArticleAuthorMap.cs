﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.BlogSiteModels
{
    public partial class ArticleAuthorMap
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int AuthorId { get; set; }

        public virtual Article Article { get; set; }
        public virtual Author Author { get; set; }
    }
}

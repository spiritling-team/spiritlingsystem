﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class ThirdProductMap
    {
        public ThirdProductMap()
        {
            ThirdPartyLoginTokens = new HashSet<ThirdPartyLoginToken>();
        }

        public byte Id { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string Memo { get; set; }

        public virtual ICollection<ThirdPartyLoginToken> ThirdPartyLoginTokens { get; set; }
    }
}

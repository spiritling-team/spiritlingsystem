﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class UserLoginHistory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Ip { get; set; }
        public string BrowserInfo { get; set; }
        public string BrowserFingerprint { get; set; }
        public DateTime CreatedTime { get; set; }

        public virtual User User { get; set; }
    }
}

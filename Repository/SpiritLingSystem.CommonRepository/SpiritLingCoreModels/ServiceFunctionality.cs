﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class ServiceFunctionality
    {
        public int Serviceid { get; set; }
        public int FunctionalityId { get; set; }
        public int Id { get; set; }

        public virtual Functionality Functionality { get; set; }
        public virtual Service Service { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class ThirdPartyLoginToken
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public byte Product { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string Memo { get; set; }

        public virtual ThirdProductMap ProductNavigation { get; set; }
        public virtual User User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class Service
    {
        public Service()
        {
            RoleServiceFunctionalities = new HashSet<RoleServiceFunctionality>();
            ServiceFunctionalities = new HashSet<ServiceFunctionality>();
        }

        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public string Memo { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual ICollection<RoleServiceFunctionality> RoleServiceFunctionalities { get; set; }
        public virtual ICollection<ServiceFunctionality> ServiceFunctionalities { get; set; }
    }
}

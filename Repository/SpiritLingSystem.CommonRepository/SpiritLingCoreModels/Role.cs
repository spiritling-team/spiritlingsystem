﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class Role
    {
        public Role()
        {
            RoleServiceFunctionalities = new HashSet<RoleServiceFunctionality>();
            UserRoles = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public string Memo { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public bool IsBuiltIn { get; set; }

        public virtual ICollection<RoleServiceFunctionality> RoleServiceFunctionalities { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class MenuFunctionalityMap
    {
        public int MenuId { get; set; }
        public int FunctionalityId { get; set; }

        public virtual Functionality Functionality { get; set; }
        public virtual MenuDisplay Menu { get; set; }
    }
}

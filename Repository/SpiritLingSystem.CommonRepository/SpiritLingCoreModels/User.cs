﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class User
    {
        public User()
        {
            ThirdPartyLoginTokens = new HashSet<ThirdPartyLoginToken>();
            UserLoginHistories = new HashSet<UserLoginHistory>();
            UserRoles = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public Guid Salt { get; set; }
        public string Password { get; set; }
        public long Phone { get; set; }
        public string Email { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ForbiddenUser ForbiddenUser { get; set; }
        public virtual ICollection<ThirdPartyLoginToken> ThirdPartyLoginTokens { get; set; }
        public virtual ICollection<UserLoginHistory> UserLoginHistories { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}

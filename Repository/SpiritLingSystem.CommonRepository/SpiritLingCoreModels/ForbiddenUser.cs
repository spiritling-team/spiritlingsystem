﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class ForbiddenUser
    {
        public int UserId { get; set; }
        public int FaultTimes { get; set; }
        public DateTime? LockedDateTime { get; set; }

        public virtual User User { get; set; }
    }
}

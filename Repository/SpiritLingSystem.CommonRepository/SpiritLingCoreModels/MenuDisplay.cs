﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class MenuDisplay
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string SortName { get; set; }
        public int Code { get; set; }
        public int? ParentCode { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public int? Order { get; set; }
        public string Description { get; set; }
        public string Memo { get; set; }

        public virtual MenuFunctionalityMap MenuFunctionalityMap { get; set; }
    }
}

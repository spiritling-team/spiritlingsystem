﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class RoleServiceFunctionality
    {
        public int? RoleId { get; set; }
        public int? ServiceId { get; set; }
        public int? FunctionalityId { get; set; }
        public int Id { get; set; }

        public virtual Functionality Functionality { get; set; }
        public virtual Role Role { get; set; }
        public virtual Service Service { get; set; }
    }
}

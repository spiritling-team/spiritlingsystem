﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SpiritLingSystem.CommonRepository.SpiritLingCoreModels
{
    public partial class ApplicationSetting
    {
        public int Id { get; set; }
        public string ConfigKey { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Memo { get; set; }
    }
}

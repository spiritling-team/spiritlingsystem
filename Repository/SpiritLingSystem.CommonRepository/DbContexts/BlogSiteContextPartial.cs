﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.CommonRepository.DbContexts
{
    public partial class BlogSiteContext : DbContext
    {
        private readonly string _connectionString;

        public BlogSiteContext()
        {
        }

        /// <summary>
        /// 手动使用using形式使用
        /// </summary>
        /// <param name="connectionString"></param>
        public BlogSiteContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// 使用startup注入方式
        /// </summary>
        /// <param name="options"></param>
        public BlogSiteContext(DbContextOptions<BlogSiteContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    optionsBuilder.UseSqlServer(_connectionString);
                }
            }
        }
    }
}

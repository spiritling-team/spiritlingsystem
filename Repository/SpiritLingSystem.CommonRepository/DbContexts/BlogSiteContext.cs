﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SpiritLingSystem.CommonRepository.BlogSiteModels;

#nullable disable

namespace SpiritLingSystem.CommonRepository.DbContexts
{
    public partial class BlogSiteContext : DbContext
    {
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<ArticleAuthorMap> ArticleAuthorMaps { get; set; }
        public virtual DbSet<ArticleClassifyMap> ArticleClassifyMaps { get; set; }
        public virtual DbSet<ArticleContent> ArticleContents { get; set; }
        public virtual DbSet<ArticleSharePassword> ArticleSharePasswords { get; set; }
        public virtual DbSet<ArticleSharePasswordHistory> ArticleSharePasswordHistories { get; set; }
        public virtual DbSet<ArticleTagMap> ArticleTagMaps { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Classify> Classifies { get; set; }
        public virtual DbSet<Sentence> Sentences { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_PRC_CI_AS");

            modelBuilder.Entity<Article>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Article_pk")
                    .IsClustered(false);

                entity.ToTable("Article");

                entity.HasIndex(e => e.Id, "Article_id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Uid, "Article_uid_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("displayName");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.IsPublic).HasColumnName("isPublic");

                entity.Property(e => e.IsSharePwd).HasColumnName("isSharePwd");

                entity.Property(e => e.PublicDate)
                    .HasColumnType("datetime")
                    .HasColumnName("publicDate");

                entity.Property(e => e.PublishEndDdate)
                    .HasColumnType("datetime")
                    .HasColumnName("publishEndDdate");

                entity.Property(e => e.PublishStartDate)
                    .HasColumnType("datetime")
                    .HasColumnName("publishStartDate");

                entity.Property(e => e.Uid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uid");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<ArticleAuthorMap>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ArticleAuthor_pk")
                    .IsClustered(false);

                entity.ToTable("ArticleAuthorMap");

                entity.HasIndex(e => e.Id, "ArticleAuthor_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId).HasColumnName("articleId");

                entity.Property(e => e.AuthorId).HasColumnName("authorId");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleAuthorMaps)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("ArticleAuthor_Article_id_fk");

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.ArticleAuthorMaps)
                    .HasForeignKey(d => d.AuthorId)
                    .HasConstraintName("ArticleAuthor_Author_id_fk");
            });

            modelBuilder.Entity<ArticleClassifyMap>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ArticleClassifyMap_pk")
                    .IsClustered(false);

                entity.ToTable("ArticleClassifyMap");

                entity.HasIndex(e => e.Id, "ArticleClassifyMap_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId).HasColumnName("articleId");

                entity.Property(e => e.ClassifyId).HasColumnName("classifyId");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleClassifyMaps)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("ArticleClassifyMap_Article_id_fk");

                entity.HasOne(d => d.Classify)
                    .WithMany(p => p.ArticleClassifyMaps)
                    .HasForeignKey(d => d.ClassifyId)
                    .HasConstraintName("ArticleClassifyMap_Classify_id_fk");
            });

            modelBuilder.Entity<ArticleContent>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ArticleContent_pk")
                    .IsClustered(false);

                entity.ToTable("ArticleContent");

                entity.HasIndex(e => e.Id, "ArticleContent_id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Uid, "ArticleContent_uid_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId).HasColumnName("articleId");

                entity.Property(e => e.Commit)
                    .HasMaxLength(512)
                    .HasColumnName("commit");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Html)
                    .IsRequired()
                    .HasColumnName("html");

                entity.Property(e => e.Markdown)
                    .IsRequired()
                    .HasColumnName("markdown");

                entity.Property(e => e.Uid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uid");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleContents)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("ArticleContent_Article_id_fk");
            });

            modelBuilder.Entity<ArticleSharePassword>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ArticleSharePassword_pk")
                    .IsClustered(false);

                entity.ToTable("ArticleSharePassword");

                entity.HasIndex(e => e.Id, "ArticleSharePassword_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId).HasColumnName("articleId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createTime");

                entity.Property(e => e.ExpireTime)
                    .HasColumnType("datetime")
                    .HasColumnName("expireTime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("password");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleSharePasswords)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("ArticleSharePassword_Article_id_fk");
            });

            modelBuilder.Entity<ArticleSharePasswordHistory>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ArticleSharePasswordHistory_pk")
                    .IsClustered(false);

                entity.ToTable("ArticleSharePasswordHistory");

                entity.HasIndex(e => e.Id, "ArticleSharePasswordHistory_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BrowserFingerprint)
                    .HasMaxLength(100)
                    .HasColumnName("browserFingerprint");

                entity.Property(e => e.BrowserInfo)
                    .HasMaxLength(200)
                    .HasColumnName("browserInfo");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.Ip)
                    .HasMaxLength(50)
                    .HasColumnName("ip");

                entity.Property(e => e.PasswordId).HasColumnName("passwordId");

                entity.HasOne(d => d.Password)
                    .WithMany(p => p.ArticleSharePasswordHistories)
                    .HasForeignKey(d => d.PasswordId)
                    .HasConstraintName("ArticleSharePasswordHistory_ArticleSharePassword_id_fk");
            });

            modelBuilder.Entity<ArticleTagMap>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ArticleTagMap_pk")
                    .IsClustered(false);

                entity.ToTable("ArticleTagMap");

                entity.HasIndex(e => e.Id, "ArticleTagMap_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId).HasColumnName("articleId");

                entity.Property(e => e.TagId).HasColumnName("tagId");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleTagMaps)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("ArticleTagMap_Article_id_fk");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.ArticleTagMaps)
                    .HasForeignKey(d => d.TagId)
                    .HasConstraintName("ArticleTagMap_Tag_id_fk");
            });

            modelBuilder.Entity<Author>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Author_pk")
                    .IsClustered(false);

                entity.ToTable("Author");

                entity.HasComment("作者表");

                entity.HasIndex(e => e.Id, "Author_id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Uid, "Author_uid_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("displayName");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Uid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uid");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Url)
                    .HasMaxLength(100)
                    .HasColumnName("url");
            });

            modelBuilder.Entity<Classify>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("classify_pk")
                    .IsClustered(false);

                entity.ToTable("Classify");

                entity.HasComment("文章分类");

                entity.HasIndex(e => e.Id, "classify_id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "classify_name_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Uid, "classify_uid_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BgColor)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("bgColor")
                    .HasDefaultValueSql("('rgba(0,0,0,1)')");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.Remark)
                    .HasMaxLength(512)
                    .HasColumnName("remark");

                entity.Property(e => e.TextColor)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("textColor")
                    .HasDefaultValueSql("('rgba(255,255,255,1)')");

                entity.Property(e => e.Uid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uid");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Url)
                    .HasMaxLength(100)
                    .HasColumnName("url");
            });

            modelBuilder.Entity<Sentence>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("sentence");

                entity.Property(e => e.Author)
                    .HasMaxLength(255)
                    .HasColumnName("author");

                entity.Property(e => e.Content)
                    .HasMaxLength(255)
                    .HasColumnName("content");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Tag_pk")
                    .IsClustered(false);

                entity.ToTable("Tag");

                entity.HasIndex(e => e.Id, "Tag_id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "Tag_name_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Uid, "Tag_uid_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BgColor)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("bgColor")
                    .HasDefaultValueSql("('rgba(0,0,0,1)')");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.TextColor)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("textColor")
                    .HasDefaultValueSql("('rgba(255,255,255,1)')");

                entity.Property(e => e.Uid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uid");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Url)
                    .HasMaxLength(100)
                    .HasColumnName("url");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

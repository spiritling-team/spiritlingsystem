﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpiritLingSystem.CommonRepository.DbContexts
{
    public partial class SpiritLingCoreContext : DbContext
    {
        private readonly string _connectionString;

        public SpiritLingCoreContext()
        {
        }

        /// <summary>
        /// 手动使用using形式使用
        /// </summary>
        /// <param name="connectionString"></param>
        public SpiritLingCoreContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// 使用startup注入方式
        /// </summary>
        /// <param name="options"></param>
        public SpiritLingCoreContext(DbContextOptions<SpiritLingCoreContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    optionsBuilder.UseSqlServer(_connectionString);
                }
            }
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SpiritLingSystem.CommonRepository.SpiritLingCoreModels;

#nullable disable

namespace SpiritLingSystem.CommonRepository.DbContexts
{
    public partial class SpiritLingCoreContext : DbContext
    {

        public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }
        public virtual DbSet<ForbiddenUser> ForbiddenUsers { get; set; }
        public virtual DbSet<Functionality> Functionalities { get; set; }
        public virtual DbSet<MenuDisplay> MenuDisplays { get; set; }
        public virtual DbSet<MenuFunctionalityMap> MenuFunctionalityMaps { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleServiceFunctionality> RoleServiceFunctionalities { get; set; }
        public virtual DbSet<Sentence> Sentences { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServiceFunctionality> ServiceFunctionalities { get; set; }
        public virtual DbSet<ThirdPartyLoginToken> ThirdPartyLoginTokens { get; set; }
        public virtual DbSet<ThirdProductMap> ThirdProductMaps { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserLoginHistory> UserLoginHistories { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_PRC_CI_AS");

            modelBuilder.Entity<ApplicationSetting>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ApplicationSetting_pk")
                    .IsClustered(false);

                entity.ToTable("ApplicationSetting");

                entity.HasComment("程序相关设置");

                entity.HasIndex(e => e.Id, "ApplicationSetting_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ConfigKey)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("configKey");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Name)
                    .HasMaxLength(512)
                    .HasColumnName("name");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasColumnName("value");
            });

            modelBuilder.Entity<ForbiddenUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("ForbiddenUser_pk")
                    .IsClustered(false);

                entity.ToTable("ForbiddenUser");

                entity.HasComment("禁止用户，登录错误次数超出次数");

                entity.HasIndex(e => e.UserId, "ForbiddenUser_userId_uindex")
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever()
                    .HasColumnName("userId");

                entity.Property(e => e.FaultTimes).HasColumnName("faultTimes");

                entity.Property(e => e.LockedDateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("lockedDateTime");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.ForbiddenUser)
                    .HasForeignKey<ForbiddenUser>(d => d.UserId)
                    .HasConstraintName("ForbiddenUser_user_id_fk");
            });

            modelBuilder.Entity<Functionality>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Functionality_pk")
                    .IsClustered(false);

                entity.ToTable("Functionality");

                entity.HasComment("功能性ID，用于权限控制");

                entity.HasIndex(e => e.Code, "Functionality_code_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "Functionality_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasComment("functuinality code number");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<MenuDisplay>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("MenuDisplay_pk")
                    .IsClustered(false);

                entity.ToTable("MenuDisplay");

                entity.HasIndex(e => e.Code, "MenuDisplay_code_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "MenuDisplay_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.Description)
                    .HasMaxLength(512)
                    .HasColumnName("description");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("displayName");

                entity.Property(e => e.Icon)
                    .HasMaxLength(100)
                    .HasColumnName("icon");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Order).HasColumnName("order");

                entity.Property(e => e.ParentCode).HasColumnName("parentCode");

                entity.Property(e => e.SortName)
                    .HasMaxLength(100)
                    .HasColumnName("sortName");

                entity.Property(e => e.Url)
                    .HasMaxLength(100)
                    .HasColumnName("url");
            });

            modelBuilder.Entity<MenuFunctionalityMap>(entity =>
            {
                entity.HasKey(e => e.MenuId)
                    .HasName("MenuFunctionalityMap_pk")
                    .IsClustered(false);

                entity.ToTable("MenuFunctionalityMap");

                entity.HasIndex(e => e.MenuId, "MenuFunctionalityMap_menuId_uindex")
                    .IsUnique();

                entity.Property(e => e.MenuId)
                    .ValueGeneratedNever()
                    .HasColumnName("menuId");

                entity.Property(e => e.FunctionalityId)
                    .HasColumnName("functionalityId")
                    .HasComment("默认0，则登录用户即可访问");

                entity.HasOne(d => d.Functionality)
                    .WithMany(p => p.MenuFunctionalityMaps)
                    .HasForeignKey(d => d.FunctionalityId)
                    .HasConstraintName("MenuFunctionalityMap_Functionality_id_fk");

                entity.HasOne(d => d.Menu)
                    .WithOne(p => p.MenuFunctionalityMap)
                    .HasForeignKey<MenuFunctionalityMap>(d => d.MenuId)
                    .HasConstraintName("MenuFunctionalityMap_MenuDisplay_id_fk");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Role_pk")
                    .IsClustered(false);

                entity.ToTable("Role");

                entity.HasComment("用户角色");

                entity.HasIndex(e => e.Id, "Role_id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Uid, "Role_uid_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.IsBuiltIn).HasColumnName("isBuiltIn");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.Uid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uid");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<RoleServiceFunctionality>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("RoleServiceFunctionality_pk")
                    .IsClustered(false);

                entity.ToTable("RoleServiceFunctionality");

                entity.HasIndex(e => e.Id, "RoleServiceFunctionality_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FunctionalityId).HasColumnName("functionalityId");

                entity.Property(e => e.RoleId).HasColumnName("roleId");

                entity.Property(e => e.ServiceId).HasColumnName("serviceId");

                entity.HasOne(d => d.Functionality)
                    .WithMany(p => p.RoleServiceFunctionalities)
                    .HasForeignKey(d => d.FunctionalityId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("RoleServiceFunctionality_Functionality_id_fk");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleServiceFunctionalities)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("RoleServiceFunctionality_Role_id_fk");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.RoleServiceFunctionalities)
                    .HasForeignKey(d => d.ServiceId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("RoleServiceFunctionality_Service_id_fk");
            });

            modelBuilder.Entity<Sentence>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Sentence_pk")
                    .IsClustered(false);

                entity.ToTable("Sentence");

                entity.HasIndex(e => e.Id, "Sentence_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Author)
                    .HasMaxLength(255)
                    .HasColumnName("author");

                entity.Property(e => e.Content)
                    .HasMaxLength(255)
                    .HasColumnName("content");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("Service_pk")
                    .IsClustered(false);

                entity.ToTable("Service");

                entity.HasIndex(e => e.Code, "Service_code_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "Service_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<ServiceFunctionality>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ServiceFunctionality_pk")
                    .IsClustered(false);

                entity.ToTable("ServiceFunctionality");

                entity.HasIndex(e => e.Id, "ServiceFunctionality_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FunctionalityId).HasColumnName("functionalityId");

                entity.Property(e => e.Serviceid).HasColumnName("serviceid");

                entity.HasOne(d => d.Functionality)
                    .WithMany(p => p.ServiceFunctionalities)
                    .HasForeignKey(d => d.FunctionalityId)
                    .HasConstraintName("ServiceFunctionality_Functionality_id_fk");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ServiceFunctionalities)
                    .HasForeignKey(d => d.Serviceid)
                    .HasConstraintName("ServiceFunctionality_Service_id_fk");
            });

            modelBuilder.Entity<ThirdPartyLoginToken>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ThirdPartyToken_pk")
                    .IsClustered(false);

                entity.ToTable("ThirdPartyLoginToken");

                entity.HasIndex(e => e.Id, "ThirdPartyToken_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AccessToken)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("accessToken");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");

                entity.Property(e => e.Product).HasColumnName("product");

                entity.Property(e => e.RefreshToken)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("refreshToken");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.ProductNavigation)
                    .WithMany(p => p.ThirdPartyLoginTokens)
                    .HasForeignKey(d => d.Product)
                    .HasConstraintName("ThirdPartyLoginToken_ThirdProductMap_id_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ThirdPartyLoginTokens)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("ThirdPartyToken_user_id_fk");
            });

            modelBuilder.Entity<ThirdProductMap>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("ThirdProductMap_pk")
                    .IsClustered(false);

                entity.ToTable("ThirdProductMap");

                entity.HasComment("第三方登录映射");

                entity.HasIndex(e => e.Id, "ThirdProductMap_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasMaxLength(512)
                    .HasColumnName("description");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("displayName");

                entity.Property(e => e.Memo)
                    .HasMaxLength(512)
                    .HasColumnName("memo");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("user_pk")
                    .IsClustered(false);

                entity.ToTable("User");

                entity.HasIndex(e => e.Uid, "User_uid_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.Id, "user_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("email");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .HasMaxLength(300)
                    .HasColumnName("password")
                    .HasComment("spirit{salt}ling{password}");

                entity.Property(e => e.Phone).HasColumnName("phone");

                entity.Property(e => e.Salt).HasColumnName("salt");

                entity.Property(e => e.Uid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("uid");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("updatedTime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<UserLoginHistory>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("UserLoginHistory_pk")
                    .IsClustered(false);

                entity.ToTable("UserLoginHistory");

                entity.HasIndex(e => e.Id, "UserLoginHistory_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BrowserFingerprint)
                    .HasMaxLength(100)
                    .HasColumnName("browserFingerprint");

                entity.Property(e => e.BrowserInfo)
                    .HasMaxLength(200)
                    .HasColumnName("browserInfo");

                entity.Property(e => e.CreatedTime)
                    .HasColumnType("datetime")
                    .HasColumnName("createdTime");

                entity.Property(e => e.Ip)
                    .HasMaxLength(50)
                    .HasColumnName("ip");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasComment("Implicit association of user tables");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserLoginHistories)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("UserLoginHistory_User_id_fk");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("UserRole_pk")
                    .IsClustered(false);

                entity.ToTable("UserRole");

                entity.HasIndex(e => e.Id, "UserRole_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.RoleId).HasColumnName("roleId");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("UserRole_Role_id_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("UserRole_user_id_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
